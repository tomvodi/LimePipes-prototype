/**
 * @file
 * @author  Thomas Baumann <teebaum@ymail.com>
 *
 * @section LICENSE
 *
 * <h3>GNU General Public License version 3</h3>
 * This program is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, see <http://www.gnu.org/licenses/>.
 *
 */
#ifndef EMBELLISHMENT_H
#define EMBELLISHMENT_H

#include "melodysymbol.h"
#include "itemtypes.h"
#include <QHash>
class QSvgRenderer;
class QGraphicsSvgItem;

class Pitch;

/*!
  * @brief The class for all Embellishments like single gracenotes, doublings, grips,...
  * @todo Use strategy pattern to change the embellishments at runtime. => Doubling behaviour,
  * grip behaviour, etc.
  */
class Embellishment : public MelodySymbol
{
Q_OBJECT
public:
    Embellishment( const PitchList *pitchList, const QPen *pen );

    //! Custom Item Typ itemtypes.h
    enum { Type = EmbellishmentType };
    //! Reimplemented from QGraphicsItem
    int type() const { return Type; }

    /*! The embellishment has a regular appearance, no alternative.
      * e.g. Regular Doubling
      */
    enum{ Regular = -1 };

    //! Reimplemented from QGraphicsItem
    QRectF boundingRect() const;

    //! Reimplemented from QGraphicsItem
    void paint( QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    //! Reimplemented from QGraphicsItem
    void mousePressEvent(QGraphicsSceneMouseEvent *event);

    //! Reimplemented from QGraphicsItem
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);

    //! Reimplemented from QGraphicsItem
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

    //! Reimplemented from QGraphicsItem
    void keyPressEvent(QKeyEvent *event);

    //! Reimplemented from QGraphicsItem
    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);

    void focusOutEvent(QFocusEvent *event);

    //! Only for debug */
    void debugPitches() const;

    //! Reimplemented from Symbol */
    void setPitch(const Pitch *pitch);

    const Pitch *followPitch(){
        return m_followPitch;
    }
    const Pitch *preceedPitch() {
        return m_preceedPitch;
    }

public slots:
    void setFollowPitch( const Pitch *pitch );
    void setPreceedPitch( const Pitch *pitch );
    void updateAlternativeFromMenu(QAction *action);
    void setAlternative(int type);


protected:
    /*! The "context" of the embellishment, the pitchList */
    const PitchList *m_pitchList;
    /*! The y-positions of the single gracenotes in the embellishment */
    QVector<qreal> m_pitches;
    /*! The pitch of the following note */
    const Pitch *m_followPitch;
    /*! The pitch of the preceeding note */
    const Pitch *m_preceedPitch;
    /*! Updates the pitches of the embellishment. */
    virtual void updateEmbellishment() = 0;
    /*! Updates the length of the m_pitches vector to the desired length */
    void changePitchesCountTo(int cnt);
    /*! Update the boundingRect according to number of single gracenotes */
    void updateBoundingRect();
    /*! Something wrong with preceeding or following Pitch */
    bool m_neighborsOk;
    /*! Subclasses can have alternatives (e.g. Doublings have an alternative: Halfdoubling).
      * Every Subclass has to add a key-value-pair for Embellishment::Regular, if it has
      * some alternatives.
      */
    QMap<int, QString>m_alternatives;
    /*! The alternative */
    int m_alternative;
 
private:
    /*! The pen for drawing */
    const QPen *m_pen;
    /*! The bounding rect */
    QRectF m_rect;

    /*! The m_svgRenderer is loaded with the needed svg-file. */
    QSvgRenderer *m_svgRenderer;

    /*! The bounds for a single gracenote head */
    QRectF m_graceRect;
    /*! The bounds for a single gracenote head with line through */
    QRectF m_graceWithLineRect;
    /*! The bounds for a flag of a single gracenote */
    QRectF m_flagRect;
    /*! The width of every single gracenote */
    qreal m_graceWidth;
    /*! The height of every single gracenote */
    qreal m_graceHeight;
    /*! The space between every single gracenote */
    qreal m_graceSpace;
    /*! The position of the uppermost horizontal line */
    qreal m_embellishTop;
    /*! The complete additional space for the line through the grace-notehead */
    qreal m_spaceForLine;

    /*! Paint a gracenote at a specific position. The vertical stem is also painted,
      * because in paintStems, the connection-points have to be calculated.
      @param painter The painter
      @param leftEdge The left edge of the gracenote
      @param yPos The center point of the gracenote
      */
    void paintGrace(QPainter *painter, qreal leftEdge, qreal yPos, QString &addition);
    /*! Paints the horizontal stems. */
    void paintStems(QPainter *painter);
    /*! Returns true, if the pitch with that y-coordinate has a line through its head. */
    bool hasLineThroughHead(qreal y) const;

    /*! Gets the bounds for a specific Id in the svg-file, loaded by m_svgRenderer */
    QRectF getBoundsForId(QString &id, qreal targetWidth = 0.0, qreal targetHeight = 0.0);

    /*! The Contextmenu actions for the Subclass */
    QList<QAction *> *m_contextMenuActions;

    void createActions();
};

#endif // EMBELLISHMENT_H
