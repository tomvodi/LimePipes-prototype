/**
 * @file
 * @author  Thomas Baumann <teebaum@ymail.com>
 *
 * @section LICENSE
 *
 * <h3>GNU General Public License version 3</h3>
 * This program is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, see <http://www.gnu.org/licenses/>.
 *
 */
#ifndef DOUBLING_H
#define DOUBLING_H

#include "embellishment.h"
#include "itemtypes.h"

class Doubling : public Embellishment
{
public:
    Doubling( const PitchList *pitchList, const QPen *pen );
    //! Custom Item Typ itemtypes.h
    enum { Type = DoublingType };
    //! Reimplemented from QGraphicsItem
    int type() const { return Type; }
    enum { Half };
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

private:
    /*! Reimplemented from Embellishment */
    void updateEmbellishment();
    /*! Returns the number of single Gracenotes the Doubling will have */
    int getPitchesCnt();
    /*! Checks, if the preceeding Pitch can be played with the following pitch */
    bool isPreceedPitchValid();
};

#endif // DOUBLING_H
