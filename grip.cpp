/**
 * @file
 * @author  Thomas Baumann <teebaum@ymail.com>
 *
 * @section LICENSE
 *
 * <h3>GNU General Public License version 3</h3>
 * This program is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, see <http://www.gnu.org/licenses/>.
 *
 */
#include "grip.h"
#include "pitch.h"
#include "ghbpitchlist.h"

Grip::Grip( const PitchList *pitchList, const QPen *pen )
    :Embellishment(pitchList, pen)
{
}

void Grip::updateEmbellishment()
{
    if(m_pitches.empty()){ //Some default values
        m_pitches.append( m_pitchList->getPitch(GHBPitchList::LowG)->y() );
        m_pitches.append( m_pitchList->getPitch(GHBPitchList::D)->y() );
        m_pitches.append( m_pitchList->getPitch(GHBPitchList::LowG)->y() );
    }

    if((m_followPitch != 0) ||
       (m_preceedPitch != 0)){
        if( m_preceedPitch != 0 && m_preceedPitch->id() == GHBPitchList::LowG){
            m_neighborsOk = false;
            //! @todo This message should be processed by the message class http://dev.limepipes.org/issues/5
            QString message("A Low G can't be played after a Grip");
            setToolTip(message);
        } else if (m_preceedPitch == 0){
            m_neighborsOk = false;
            QString message;
            message.append("Grip has no preceeding MelodyNote");
            setToolTip(message);
        } else if (m_followPitch == 0){
            m_neighborsOk = false;
            QString message;
            message.append("Grip has no following MelodyNote");
            setToolTip(message);
        } else {
            m_neighborsOk = true;
            setToolTip(false);
        }

        if(m_followPitch != 0 ) {
            int followPitchId = m_followPitch->id();
            switch( followPitchId ) {
            case GHBPitchList::LowG:
                m_neighborsOk = false;
                if( m_preceedPitch != 0 && m_preceedPitch->id() == GHBPitchList::LowG){
                    setToolTip("Grip can't be played between two Low G-Melodynotes");
                } else {
                    setToolTip("A Low G can't be played after a Grip");
                }
                break;
            case GHBPitchList::LowA:
            case GHBPitchList::B:
            case GHBPitchList::C:
            case GHBPitchList::E:
            case GHBPitchList::F:
            case GHBPitchList::HighG:
            case GHBPitchList::HighA:
                m_pitches.replace(1, m_pitchList->getPitch(GHBPitchList::D)->y());
                break;
            case GHBPitchList::D:
                m_pitches.replace(1, m_pitchList->getPitch(GHBPitchList::B)->y());
                break;
            }
        } else { //No following pitch
                    m_pitches.replace(0, m_pitchList->getPitch(GHBPitchList::LowG)->y());
                    m_pitches.replace(1, m_pitchList->getPitch(GHBPitchList::D)->y());
                    m_pitches.replace(2, m_pitchList->getPitch(GHBPitchList::LowG)->y());
        }
    } else if (m_preceedPitch == 0) {
        m_neighborsOk = false;
        setToolTip("Grip has no preceeding melodynote.");
    } else if(m_followPitch == 0){
        m_neighborsOk = false;
        setToolTip("Grip has no following melodynote.");
    }
    Embellishment::updateBoundingRect();
}

void Grip::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Embellishment::paint(painter, option, widget);
}
