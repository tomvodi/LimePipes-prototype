/**
 * @file
 * @author  Thomas Baumann <teebaum@ymail.com>
 *
 * @section LICENSE
 *
 * <h3>GNU General Public License version 3</h3>
 * This program is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, see <http://www.gnu.org/licenses/>.
 *
 */
#include "melodynote.h"
#include "melodysymbol.h"
#include "symbol.h"
#include "pitch.h"
#include <QGraphicsScene>
#include <QSvgRenderer>
#include <QtGui>
#include <QDebug>

MelodyNote::MelodyNote(const PitchList *pitchList, const QPen *pen, const Pitch *pitch, const NoteLength::Length length)
    :MelodySymbol(pitchList, pen)
{
    m_pitch = pitch;
    m_rect = QRectF();
    m_length = new NoteLength(length);
    m_svgRenderer = new QSvgRenderer(QString(":/mainwindow/images/melody_notes_and_flags.svg"), this);
    m_noteHeadIds = new QHash<int, QString>;
    m_noteHeadIds->insert(FilledHead, QString("melody_fill"));
    m_noteHeadIds->insert(NoFilledHead, QString("melody_hole"));
    m_noteHeadIds->insert(FilledHeadLine, QString("melody_fill_line"));
    m_noteHeadIds->insert(NoFilledHeadLine, QString("melody_hole_line"));
    m_noteHeadIds->insert(Dot, QString("melody_dot"));
    m_connectionOffset = 0.13267;  //Get the value from the svg-file
    m_dots = 0;

    setRectForPitch();
    setSizeHintsForPitch();

    setFlags( QGraphicsItem::ItemIsFocusable |
              QGraphicsItem::ItemIsSelectable );
    setCacheMode(QGraphicsItem::DeviceCoordinateCache);
}

bool MelodyNote::hasLineThroughHead(const Pitch *pitch) const
{
    if(pitch->name() == QString("High A")){
        return true;
    }
    return false;
}

bool MelodyNote::isHeadFilled(const NoteLength *length) const
{
    if(length->length() == NoteLength::Whole || length->length() == NoteLength::Half)
    {
        return false;
    }
    return true;
}

void MelodyNote::setPitch(const Pitch *pitch)
{
    if(m_pitch->y() != pitch->y()){
        m_pitch = pitch;
        emit pitchHasChanged(m_pitch);
    }

    setRectForPitch();
    setSizeHintsForPitch();
    setConnectionPoints();
}

void MelodyNote::setRectForPitch()
{
    //Set Pitch and update bounding Rect
    m_rect = getRectForMelodyNote();
}

void MelodyNote::setSizeHintsForPitch()
{
    qreal width = m_rect.width();
    qreal height = m_rect.height();

    if(m_dots){
        width += getBoundingForDots().width();
    }

    setMinimumSize(width, height);
    setPreferredSize(width, height);
    setMaximumSize(width, height);
}

void MelodyNote::setConnectionPoints()
{
    //! @todo Store connection Points in item coordinates and use mapFromItem in the stemDrawer.<br />
    //! Issue: http://dev.limepipes.org/issues/11
    QRectF t_rect = mapFromParent(geometry()).boundingRect();
    //Values of the connections
    qreal left_offset = 0.0; //Line through head
    if(hasLineThroughHead(m_pitch)){  //Line through head => notehead a bit more to the right
        left_offset += m_connectionOffset*m_rect.width();
    }
    m_leftConnection = mapToParent( QPointF(left_offset + t_rect.left()+1,
                                            m_pitch->y() + 0.2 * m_pitch->lineHeight()) );
}

void MelodyNote::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
#ifdef QT_DEBUG
    /*
    //bounding Rect
    painter->setPen( QPen(Qt::green, 1.0) );
    painter->setBrush( Qt::transparent );
    painter->drawRect( boundingRect() );

    //geometry Rect
    painter->setPen( QPen(Qt::darkMagenta, 1.0) );
    painter->setBrush( Qt::transparent );
    painter->drawPolygon( mapFromParent(geometry()) );

    //Shape
    painter->setPen( QPen(Qt::green, 1.0) );
    painter->drawPath( shape() );


    //koordinatensystem
    painter->setPen(QPen(Qt::green, 1.0));
    painter->drawLine(-20, 0, 20, 0);
    painter->drawLine(0, -20, 0, 20);
    painter->setPen(Qt::NoPen);
    //ende koordinatensystem
*/
#endif
    //Notehead
    QString t_headId(m_noteHeadIds->value(getNoteHeadType()));
    t_headId += getImageAddition();
    QRectF t_headRect(getRectForMelodyNote());
    m_svgRenderer->render(painter, t_headId, t_headRect);
    for(int i=0; i<m_dots; i++){
        m_svgRenderer->render(painter, m_noteHeadIds->value(Dot) + getImageAddition(), getRectForDot(i+1) );
    }
}

void MelodyNote::setLength(NoteLength *length)
{
    if(m_length->length() != length->length()){
        m_length = length;
        emit lengthHasChanged();
    }
}

QRectF MelodyNote::boundingRect() const
{
    qreal hpw = m_pen->width() / 2;
    QRectF t_rect = m_rect;
    t_rect = t_rect.united(getBoundingForDots());
    return t_rect.adjusted( -hpw, -hpw, hpw, hpw );
}

QPainterPath MelodyNote::shape() const
{
    QPainterPath path;
    path.addRect(m_rect);
    return path;
}

void MelodyNote::mousePressEvent( QGraphicsSceneMouseEvent *event )
{
    if(hovermode()){
        event->ignore();
        return;
    }
    setFocus(Qt::MouseFocusReason);
    update();
    m_dragStartY = event->pos().y();
    m_dragStartX = event->pos().x();
}

void MelodyNote::mouseReleaseEvent( QGraphicsSceneMouseEvent *event )
{
    if(hovermode()){
        event->ignore();
        return;
    }
}

void MelodyNote::mouseMoveEvent( QGraphicsSceneMouseEvent *event )
{
    if(hovermode()){
        return;
    }
    qreal yPos = event->pos().y();
    qreal ydist = m_dragStartY - yPos;
    qreal nextPitchDist = m_pitch->lineHeight() / 2;

    qreal xPos = event->pos().x();
    qreal xdist = m_dragStartX - xPos;
    qreal nextLengthDist = m_pitch->lineHeight(); //ratio for changing pitch is the same for changing notelength

    //up and down -- Pitch
    if( ydist > 0 ) //up
    {
        if( ydist > (nextPitchDist/2) ){
            if(m_pitch->nextHigher() != 0){
                m_dragStartY -= nextPitchDist;
                prepareGeometryChange();
                setPitch( m_pitch->nextHigher() );
                emit pitchHasChanged(m_pitch);
                //qDebug() << "MelodyNote - New Pitch: " << m_pitch->name();
            }
        }
    } else if( ydist < 0 ) { //down
        if( -ydist > (nextPitchDist/2) ){
            if(m_pitch->nextLower() != 0){
                m_dragStartY += nextPitchDist;
                prepareGeometryChange();
                setPitch( m_pitch->nextLower() );
                emit pitchHasChanged(m_pitch);
                //qDebug() << "MelodyNote - New Pitch: " << m_pitch->name();
            }
        }
    }

    //left and right -- Length
    if( xdist > 0 ) //left
    {
        if( xdist > (nextLengthDist) ){
            if( m_length->length() != NoteLength::Thirtysecond ){
                m_dragStartX -= nextLengthDist;
            }
            (*m_length)--;
            emit lengthHasChanged();
            //qDebug() << "MelodyNote - New Length: " << m_length->length();
            update();
        }
    } else if( xdist < 0 ) { //right
        if( -xdist > (nextLengthDist) ){
            if( m_length->length() != NoteLength::Whole ){
                m_dragStartX += nextLengthDist;
            }
            (*m_length)++;
            emit lengthHasChanged();
            //qDebug() << "MelodyNote - New Length: " << m_length->length();
            update();
        }
    }
}

QSizePolicy MelodyNote::sizePolicy() const
{
    return QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
}

QPointF MelodyNote::leftConnection() const
{
    return m_leftConnection;
}


QPointF MelodyNote::rightConnection() const
{
    return m_rightConnection;
}

void MelodyNote::setGeometry(const QRectF &rect)
{
    QGraphicsWidget::setGeometry(rect);
    QPointF t_leftConnection = m_leftConnection;
    setConnectionPoints();
    if(t_leftConnection != m_leftConnection){
        emit pitchHasChanged(m_pitch);
    }
}

QRectF MelodyNote::getRectForMelodyNote()
{
    QRectF t_rect;
    QSizeF t_size = m_svgRenderer->boundsOnElement(m_noteHeadIds->value(getNoteHeadType())).size(); //Defaultsize of notehead
    qreal heightToWidth = (qreal)t_size.height() / t_size.width(); //proportion of notehead
    qreal targetHeight = (qreal)m_pitch->lineHeight();
    qreal targetWidth = targetHeight / heightToWidth;
    t_rect = QRectF(0, m_pitch->y()-m_pitch->lineHeight()/2, targetWidth, targetHeight);
    return t_rect;
}

QRectF MelodyNote::getRectForDot(int dot) const
{
    //Space between dots. Also the space between the MelodyNote and the first Dot.
    //Has the MelodyNote a Pitch on a line, there is no space
    qreal t_space = m_connectionOffset * m_rect.width();
    QRectF t_rect(0.0, 0.0, 0.0, 0.0);

    QSizeF t_size = m_svgRenderer->boundsOnElement(m_noteHeadIds->value(Dot)).size(); //Defaultsize of one dot
    qreal heightToWidth = (qreal)t_size.height() / t_size.width(); //proportion of dot
    qreal targetHeight = (qreal)m_pitch->lineHeight()/2.5;
    qreal targetWidth = targetHeight / heightToWidth;
    t_rect = QRectF(0.0, 0.0, targetWidth, targetHeight);

    qreal dotY = m_pitch->y()-t_rect.height()/2;
    if(m_pitch->onLine()){ //Is Pitch on a line => draw dots above line
        dotY -= m_pitch->lineHeight()/2.3;
    }
    t_rect.moveTopLeft(QPointF(m_rect.right(), dotY));  //set directly after melodyNote
    if(!hasLineThroughHead(m_pitch)){
        t_rect.moveLeft(t_rect.left()+t_space);
    }

    if(dot == 1){
         return t_rect;
     }
    if(dot == 2){
        t_rect.moveLeft(t_rect.right() + t_space);
         return t_rect;
    }
    return t_rect;
}

QRectF MelodyNote::getBoundingForDots() const
{
    QRectF t_rect(0.0, 0.0, 0.0, 0.0);
    for(int i=0; i<m_dots; i++){
        if(i==0){ //first dot
            t_rect = getRectForDot(1);
            if(m_rect.right() != t_rect.left()){ //Space was added before first dot
                t_rect.adjust(m_rect.right()-t_rect.left(), 0.0, 0.0, 0.0);
            }
        }
        if(i==1){ //second dot
            t_rect = t_rect.united(getRectForDot(2));
        }
    }
    return t_rect;
}

int MelodyNote::getNoteHeadType() const
{
    int t_headType;
    if(isHeadFilled(m_length)){
        t_headType = FilledHead;
        if(hasLineThroughHead(m_pitch)){
            t_headType = FilledHeadLine;
        }
    } else {
        t_headType = NoFilledHead;
        if(hasLineThroughHead(m_pitch)){
            t_headType = NoFilledHeadLine;
        }
    }
    return t_headType;
}

void MelodyNote::addDot()
{
    m_dots == 2 ? m_dots = 0 : m_dots++;
    prepareGeometryChange();
    setSizeHintsForPitch();
}

void MelodyNote::keyPressEvent(QKeyEvent *event)
{
    //qDebug() << "Melody Note - key press Event";
    if(event->key() == Qt::Key_Period){ //Dot on keyboard
        addDot();
    } else {
        event->ignore();
    }
}

QString MelodyNote::getImageAddition()
{
    QString addition("");
    if(hovermode()){
        addition = "_hov";
    } else {
        if(hasFocus()){
            addition = "_foc";
        }
    }
    return addition;
}

void MelodyNote::focusOutEvent(QFocusEvent *event)
{
    update();
}
