/**
 * @file
 * @author  Thomas Baumann <teebaum@ymail.com>
 *
 * @section LICENSE
 *
 * <h3>GNU General Public License version 3</h3>
 * This program is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, see <http://www.gnu.org/licenses/>.
 *
 */
#ifndef TIMESIGNATURE_H
#define TIMESIGNATURE_H

#include "symbol.h"
#include "itemtypes.h"
#include <QHash>

class PitchList;
class QSvgRenderer;

class TimeSignature : public Symbol
{
    Q_OBJECT
public:
    enum { Type = TimeSignatureType };
    int type() const { return Type; }
    enum timeSigType{ _2_2, _2_4, _3_4, _4_4, _3_8, _6_8, _9_8, _12_8 };
    TimeSignature(const PitchList *pitchList, const QPen *pen);
    ~TimeSignature();
    void setTimeSig(int clefType);
    int timeSigType() const {
        return m_timeSig;
    }
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect();
signals:
    void timeSignatureChanged();
private:
    /*! The actual timeSigType */
    int m_timeSig;
    /*! The m_svgRenderer is loaded with the svg-file, including all time-signature
      * symbols in it. Every type of TimeSignature has its own Id in that file.
      */
    QSvgRenderer *m_svgRenderer;
    /*! For every clefType, the associated Id in the svg-file is mapped */
    QHash<int, QString> *m_timeSignatureIds;
    /*! Returns the bounds for the actual timesignature */
    QRectF getRectForTimeSig();
};

#endif // TIMESIGNATURE_H
