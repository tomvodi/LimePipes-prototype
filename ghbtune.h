/**
 * @file
 * @author  Thomas Baumann <teebaum@ymail.com>
 *
 * @section LICENSE
 *
 * <h3>GNU General Public License version 3</h3>
 * This program is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, see <http://www.gnu.org/licenses/>.
 *
 */
#ifndef GHBTUNE_H
#define GHBTUNE_H

#include <QList>
#include <QVector>
#include <QHash>
#include <QGraphicsWidget>
#include <QActionGroup>
#include "itemtypes.h"

class QGraphicsScene;
class QGraphicsLinearLayout;
class QGraphicsDropShadowEffect;

class PitchList;
class MusicBar;
class Symbol;
class TimeSignature;
class QActionGroup;
class QAction;

/*! @class GHBTune
  * @brief This class represents a Tune for the Great Highland Bagpipes.
  * @todo Actually this class handles the layout for the MusicBars. This has to
  * be done by another class.
  */
class GHBTune : public QGraphicsWidget
{
    Q_OBJECT
public:
    /*! @brief Constructor
      * @param scene The QGraphicsScene to add the Tune
      */
    GHBTune( QGraphicsScene *scene );
    /*! @brief Destructor */
    ~GHBTune();
    /*! @brief Reimplemented from QGraphicsItem::boundingRect() */
    QRectF boundingRect() const;
    /*! @brief Reimplemented from QGraphicsLayoutItem */
    QSizePolicy sizePolicy() const;
    /*! Returns all actions for the possible symbols */
    const QActionGroup *getActions();
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void setGeometry(const QRectF &rect);
    void keyPressEvent(QKeyEvent *event);
    void setTimeSignature(int type);

public slots:
    /*! Sets the symbol cursor for the action. */
    void setCursorForAction(QAction *action);
    /*! Just for debugging */
    void debugSlot(bool tf);
    /*! Adds a Part. It consists of two rows. The first row with upbeat */
    void addPart();
    /*! Sets the timeSignature for the whole tune (all bars) */
    void updateTimeSig(QAction *action);

private:
    void createActions();
    void createConnections();
    /*! @brief The music bars of the Tune */
    QVector<MusicBar *> m_bars;
    /*! @brief The Pitch List of the Tune. Here GHBPitchList */
    PitchList *m_pitchList;
    /*! @brief A vertical Layout for the tune */
    QGraphicsLinearLayout *m_vlayout;
    /*! @brief The bounding rect */
    QRectF m_rect;
    /*! @brief The pen for drawing everything in the tune. It is passed to the bars, symbols, etc. */
    QPen *m_pen;
    /*! The GraphicsScene */
    QGraphicsScene *m_scene;

    /*! Symbol-cursor: A Symbol is chosen from a menu */
    Symbol *m_cursor;

    /*! The Actions for all possible Symbols in the tune */
    QActionGroup *m_actions;

    /*! The default Symbols for the cursor */
    QHash<QString, Symbol *> m_cursorSymbols;

    QGraphicsDropShadowEffect *m_shadow;

    /*! Margins between rows and margin of the page */
    qreal m_margin;

    /*! The TimeSignature for the Tune */
    TimeSignature *m_timeSig;

    QGraphicsTextItem *m_title;
    QGraphicsTextItem *m_type;
    QGraphicsTextItem *m_composer;

    QAction *addPartAction;
    QAction *timeSig22Action;
    QAction *timeSig24Action;
    QAction *timeSig34Action;
    QAction *timeSig44Action;
    QAction *timeSig38Action;
    QAction *timeSig68Action;
    QAction *timeSig98Action;
    QAction *timeSig128Action;

    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);
    void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
    void hoverMoveEvent(QGraphicsSceneHoverEvent *event);

private slots:
    /*! Uncheck other Actions, if one is checked. Also sets the cursor to 0,
      * if the already checked action was unchecked
      */
    void updateActions(QAction *action);
};

#endif // GHBTUNE_H
