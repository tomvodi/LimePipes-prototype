/**
 * @file
 * @author  Thomas Baumann <teebaum@ymail.com>
 *
 * @section LICENSE
 *
 * <h3>GNU General Public License version 3</h3>
 * This program is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, see <http://www.gnu.org/licenses/>.
 *
 */
#include "pitchlist.h"
#include "pitch.h"
#include "limits.h"
#include "qdebug.h"

PitchList::PitchList(qreal lineHeight)
{
    m_lineHeight = lineHeight;
    m_baseLine = 0;
    m_pitches = new QHash<int, Pitch*>;
    m_cachedPositions = new QHash<int, const Pitch *>;
}

PitchList::~PitchList()
{
    delete m_pitches;
}

qreal PitchList::lineHeight() const
{
    return m_lineHeight;
}

void PitchList::setBaseLine(qreal base)
{
    m_baseLine = base;
    QHash<int, Pitch*>::const_iterator i;
    for(i=m_pitches->begin(); i != m_pitches->end(); i++){
        (*i)->setBaseLine(base);
    }
}

qreal PitchList::baseLine() const
{
    return m_baseLine;
}

const Pitch *PitchList::getPitch(int pitch) const
{
    return m_pitches->value(pitch);
}

const Pitch *PitchList::pitchForPos(int ypos) const
{
    if( m_cachedPositions->contains(ypos)){
        return m_cachedPositions->value(ypos);
    } else {
        const Pitch *t_pitch;
        int dist;
        int min_dist = INT_MAX;
        const Pitch *min_pitch;
        QHash<int, Pitch*>::const_iterator i;
        for(i=m_pitches->begin(); i != m_pitches->end(); i++){
            t_pitch = *i;
            if(t_pitch->y() >= ypos){
                dist = t_pitch->y() - ypos;
            } else {
                dist = ypos - t_pitch->y();
            }
            if(dist < min_dist){
                min_dist = dist;
                min_pitch = t_pitch;
            }
        }
        m_cachedPositions->insert(ypos, min_pitch);
        return min_pitch;
    }

}

void PitchList::cachePositions(int start, int end)
{
    for(int i=start; i<=end; i++){
        pitchForPos(i);
    }
}
