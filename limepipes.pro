# -------------------------------------------------
# Project created by QtCreator 2011-06-23T13:51:39
# -------------------------------------------------
TARGET = limepipes
TEMPLATE = app
SOURCES += main.cpp \
    mainwindow.cpp \
    musicbar.cpp \
    symbol.cpp \
    controlsymbol.cpp \
    melodysymbol.cpp \
    melodynote.cpp \
    embellishment.cpp \
    pitch.cpp \
    ghbtune.cpp \
    pitchlist.cpp \
    ghbpitchlist.cpp \
    notelength.cpp \
    stemdrawer.cpp \
    standardstemdrawer.cpp \
    doubling.cpp \
    singlegrace.cpp \
    clef.cpp \
    timesignature.cpp \
    aboutdialog.cpp \
    grip.cpp
HEADERS += mainwindow.h \
    musicbar.h \
    symbol.h \
    controlsymbol.h \
    melodysymbol.h \
    melodynote.h \
    embellishment.h \
    itemtypes.h \
    pitch.h \
    ghbtune.h \
    pitchlist.h \
    ghbpitchlist.h \
    notelength.h \
    stemdrawer.h \
    standardstemdrawer.h \
    doubling.h \
    singlegrace.h \
    clef.h \
    timesignature.h \
    aboutdialog.h \
    grip.h
FORMS += mainwindow.ui \
    about_dialog.ui
OTHER_FILES += README.txt \
    LICENSE.txt
RESOURCES += ressources.qrc
QT += svg
