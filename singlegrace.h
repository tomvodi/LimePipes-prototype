/**
 * @file
 * @author  Thomas Baumann <teebaum@ymail.com>
 *
 * @section LICENSE
 *
 * <h3>GNU General Public License version 3</h3>
 * This program is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, see <http://www.gnu.org/licenses/>.
 *
 */
#ifndef SINGLEGRACE_H
#define SINGLEGRACE_H

#include "embellishment.h"
#include "itemtypes.h"

class PitchList;

/*! @class SingleGrace
  * @brief Manages all single-gracenote-embellishments like single-graces and strikes
  */
class SingleGrace : public Embellishment
{
public:
    SingleGrace( const PitchList *pitchList, const QPen *pen, const Pitch *pitch = 0);
    //! Custom Item Typ itemtypes.h
    enum { Type = SingleGraceType };
    //! Reimplemented from QGraphicsItem
    int type() const { return Type; }

    void mousePressEvent( QGraphicsSceneMouseEvent *event );
    void mouseReleaseEvent( QGraphicsSceneMouseEvent *event );
    void mouseMoveEvent( QGraphicsSceneMouseEvent *event );

private:
    //! Reimplemented from Symbol. Single gracenotes have a pitch
    bool hasPitch() const
    {
        return true;
    }
    //! Helper variable for mousePressEvent and mouseMoveEvent while dragging the gracenote
    qreal m_dragStartY;
    /*! Reimplemented from Embellishment */
    void updateEmbellishment();
    /*! Checks, if the preceeding Pitch can be played with the following pitch */
    bool isPreceedPitchValid();
    /*! Is the gracenote a strike, that means, that the preceeding and the following
      * pitches are higher than the gracenote-pitch and they have the same pitch.
      */
    bool isStrike();
    /*! Is the gracenote a slur, that means, that the preceeding pitch is higher than the gracenote,
      * the pitch of the following melody-note is one pitch higher than the gracenote-pitch
      */
    bool isSlur();
};

#endif // SINGLEGRACE_H
