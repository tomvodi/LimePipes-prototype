/**
 * @file
 * @author  Thomas Baumann <teebaum@ymail.com>
 *
 * @section LICENSE
 *
 * <h3>GNU General Public License version 3</h3>
 * This program is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, see <http://www.gnu.org/licenses/>.
 *
 */
#include "doubling.h"
#include "pitchlist.h"
#include "pitch.h"
#include "ghbpitchlist.h"

#include <QDebug>

Doubling::Doubling( const PitchList *pitchList, const QPen *pen )
    :Embellishment(pitchList, pen)
{
    m_alternatives.insert(Embellishment::Regular, QString("Regular Doubling"));
    m_alternatives.insert(Doubling::Half, QString("Half Doubling"));
    updateEmbellishment();
    m_neighborsOk = true;
}

void Doubling::updateEmbellishment()
{
    if(m_pitches.empty()){ //Some default values
        if(m_alternative == Embellishment::Regular){
            m_pitches.append( m_pitchList->getPitch(GHBPitchList::HighG)->y() );
        }
        m_pitches.append( m_pitchList->getPitch(GHBPitchList::E)->y() );
        m_pitches.append( m_pitchList->getPitch(GHBPitchList::F)->y() );
    }

    if((m_followPitch != 0) ||
       (m_preceedPitch != 0)){
        if( m_preceedPitch != 0 && !isPreceedPitchValid()){
            m_neighborsOk = false;
            //! @todo This message should be processed by the message class http://dev.limepipes.org/issues/5
            QString message;
            message.append("A \"");
            message.append(m_preceedPitch->name());
            message.append("\"-melody note can't be played in front of a ");
            message.append(m_followPitch->name());
            message.append(" ");
            m_alternative == Doubling::Half ? message.append(m_alternatives.value(Doubling::Half))
                : message.append(m_alternatives.value(Embellishment::Regular));
            setToolTip(message);
        } else if (m_preceedPitch == 0){
            m_neighborsOk = false;
            QString message;
            message.append("Doubling has no preceeding MelodyNote");
            setToolTip(message);
        } else if (m_followPitch == 0){
            m_neighborsOk = false;
            QString message;
            message.append("Doubling has no following MelodyNote");
            setToolTip(message);
        } else {
            m_neighborsOk = true;
            setToolTip(false);
        }

        if(m_followPitch != 0 ) {
            int followPitchId = m_followPitch->id();
            changePitchesCountTo(getPitchesCnt());
            qreal preLast;
            qreal last;
            switch( followPitchId ) {
            case GHBPitchList::LowG:
                preLast = m_pitchList->getPitch(GHBPitchList::LowG)->y();
                last = m_pitchList->getPitch(GHBPitchList::D)->y();
                break;
            case GHBPitchList::LowA:
                preLast = m_pitchList->getPitch(GHBPitchList::LowA)->y();
                last = m_pitchList->getPitch(GHBPitchList::D)->y();
                break;
            case GHBPitchList::B:
                preLast = m_pitchList->getPitch(GHBPitchList::B)->y();
                last = m_pitchList->getPitch(GHBPitchList::D)->y();
                break;
            case GHBPitchList::C:
                preLast = m_pitchList->getPitch(GHBPitchList::C)->y();
                last = m_pitchList->getPitch(GHBPitchList::D)->y();
                break;
            case GHBPitchList::D:
                preLast = m_pitchList->getPitch(GHBPitchList::D)->y();
                last = m_pitchList->getPitch(GHBPitchList::E)->y();
                break;
            case GHBPitchList::E:
                preLast = m_pitchList->getPitch(GHBPitchList::E)->y();
                last = m_pitchList->getPitch(GHBPitchList::F)->y();
                break;
            case GHBPitchList::F:
                preLast = m_pitchList->getPitch(GHBPitchList::F)->y();
                last = m_pitchList->getPitch(GHBPitchList::HighG)->y();
                break;
            case GHBPitchList::HighG:
                if(m_alternative == Doubling::Half){
                    preLast = m_pitchList->getPitch(GHBPitchList::F)->y();
                    last = m_pitchList->getPitch(GHBPitchList::HighG)->y();
                    break;
                }
                preLast = m_pitchList->getPitch(GHBPitchList::HighG)->y();
                last = m_pitchList->getPitch(GHBPitchList::F)->y();
                break;
            case GHBPitchList::HighA:
                if(m_alternative == Doubling::Half){
                    preLast = m_pitchList->getPitch(GHBPitchList::F)->y();
                    last = m_pitchList->getPitch(GHBPitchList::HighG)->y();
                    break;
                }
                preLast = m_pitchList->getPitch(GHBPitchList::HighA)->y();
                last = m_pitchList->getPitch(GHBPitchList::HighG)->y();
                break;
            }
            if(m_alternative == Embellishment::Regular){
                int preceedPitchId = -1;
                if(m_preceedPitch != 0)
                    preceedPitchId = m_preceedPitch->id();
                if(followPitchId == GHBPitchList::LowG ||
                   followPitchId == GHBPitchList::LowA ||
                   followPitchId == GHBPitchList::B ||
                   followPitchId == GHBPitchList::C ||
                   followPitchId == GHBPitchList::D ||
                   followPitchId == GHBPitchList::E ||
                   followPitchId == GHBPitchList::F
                ){
                    if(preceedPitchId == GHBPitchList::HighG){
                        m_pitches.replace(0, m_pitchList->getPitch(GHBPitchList::HighA)->y()); //Thumb-Doubling
                    } else {
                        m_pitches.replace(0, m_pitchList->getPitch(GHBPitchList::HighG)->y()); //Regular-Doubling
                    }
                }
            }
            int prelastIdx = m_pitches.count()-2;
            int lastIdx = m_pitches.count()-1;
                m_pitches.replace(prelastIdx, preLast);
                m_pitches.replace(lastIdx, last);
        } else { //No following pitch
            if(m_alternative == Embellishment::Regular){
                if(m_pitches.count() != 3){
                    changePitchesCountTo(3);
                    m_pitches.replace(0, m_pitchList->getPitch(GHBPitchList::HighG)->y());
                    m_pitches.replace(1, m_pitchList->getPitch(GHBPitchList::E)->y());
                    m_pitches.replace(2, m_pitchList->getPitch(GHBPitchList::F)->y());
                }
            } else {
                if(m_pitches.count() != 2){
                    changePitchesCountTo(2);
                    m_pitches.replace(0, m_pitchList->getPitch(GHBPitchList::E)->y());
                    m_pitches.replace(1, m_pitchList->getPitch(GHBPitchList::F)->y());
                }
            }

        }
    } else if (m_preceedPitch == 0) {
        m_neighborsOk = false;
        setToolTip("Doubling has no preceeding melodynote.");
    } else if(m_followPitch == 0){
        m_neighborsOk = false;
        setToolTip("Doubling has no following melodynote.");
    }
    if(m_preceedPitch == 0 && m_followPitch == 0){
        if(m_alternative == Embellishment::Regular){
            if(m_pitches.count() != 3){
                changePitchesCountTo(3);
                m_pitches.replace(0, m_pitchList->getPitch(GHBPitchList::HighG)->y());
                m_pitches.replace(1, m_pitchList->getPitch(GHBPitchList::E)->y());
                m_pitches.replace(2, m_pitchList->getPitch(GHBPitchList::F)->y());
            }
        } else {
            if(m_pitches.count() != 2){
                changePitchesCountTo(2);
                m_pitches.replace(0, m_pitchList->getPitch(GHBPitchList::E)->y());
                m_pitches.replace(1, m_pitchList->getPitch(GHBPitchList::F)->y());
            }
        }
    }

    Embellishment::updateBoundingRect();
}

int Doubling::getPitchesCnt()
{
    if(m_alternative == Doubling::Half){
        prepareGeometryChange();
        return 2;
    }
    int cnt = 0;
    int followPitchId = -1;
    if(m_followPitch != 0)
        followPitchId = m_followPitch->id();
    switch( followPitchId ) {
    case GHBPitchList::LowG:
    case GHBPitchList::LowA:
    case GHBPitchList::B:
    case GHBPitchList::C:
    case GHBPitchList::D:
    case GHBPitchList::E:
    case GHBPitchList::F:
        cnt = 3;
        if(m_preceedPitch != 0 && m_preceedPitch->id() == GHBPitchList::HighA){ // Half Doubling
            cnt = 2;
        }
        break;
    case GHBPitchList::HighG:
    case GHBPitchList::HighA:
        cnt = 2;
        break;
    }
    prepareGeometryChange();
    return cnt;
}

bool Doubling::isPreceedPitchValid()
{
    int preceedPitch = -1;
    int followPitch = -1;
    bool valid = false;
    if(m_preceedPitch != 0){
        preceedPitch = m_preceedPitch->id();
        valid = true;
        if( m_followPitch != 0){
            followPitch = m_followPitch->id();
        }
        switch( m_alternative ){
        case Embellishment::Regular:{
                switch( followPitch ) {
                case GHBPitchList::HighG:
                    if((preceedPitch == GHBPitchList::HighG) ||
                       (preceedPitch == GHBPitchList::HighA)){
                        valid = false;
                    }
                    break;
                case GHBPitchList::HighA:
                    if((preceedPitch == GHBPitchList::HighA)){
                        valid = false;
                    }
                    break;
                }
                break;
            }
        case Doubling::Half: {
                if(m_followPitch != 0){
                    if(m_preceedPitch->y() >= m_followPitch->y()){
                        valid = false;
                        if(followPitch == GHBPitchList::HighG ||
                           followPitch == GHBPitchList::HighA){
                            valid = false;
                        }
                    }
                }
                break;
            }

        }
    }
    return valid;
}

void Doubling::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Embellishment::paint(painter, option, widget);
}
