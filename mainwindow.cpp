/**
 * @file
 * @author  Thomas Baumann <teebaum@ymail.com>
 *
 * @section LICENSE
 *
 * <h3>GNU General Public License version 3</h3>
 * This program is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, see <http://www.gnu.org/licenses/>.
 *
 */
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtGui>

//Includes for testing
#include <QDebug>

#include "ghbtune.h"
#include "musicbar.h"
#include "ghbpitchlist.h"
#include "melodynote.h"
#include "aboutdialog.h"

#include "QGraphicsTextItem"
#include "QFontDatabase"
#include <QFontMetricsF>
#include <QRectF>
#include <QGraphicsRectItem>
#include <QtOpenGL/QGLWidget>

//End Includes for Testing

#include <QGraphicsView>
#include <QGraphicsScene>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    qDebug() << QT_VERSION_STR;
    printer = new QPrinter(QPrinter::HighResolution);
    ui->setupUi(this);
    m_tune = 0;

    createViewAndScene();
    createConnections();

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::createViewAndScene()
{
    view = new QGraphicsView;
    //view->setViewport(new QGLWidget());
    scene = new QGraphicsScene(this);
    QSize pageSize = printer->paperSize(QPrinter::Point).toSize();
    view->setScene(scene);
    view->scale(0.7,0.7);
    view->setCacheMode(QGraphicsView::CacheBackground);
    view->setRenderHint( QPainter::Antialiasing );
    view->setBackgroundBrush(QApplication::palette().brush(QPalette::Window));
    setCentralWidget(view);
    delete ui->centralWidget;  /*! Delete the central widget before we set it to the view */
    //newTune();
}

void MainWindow::createConnections()
{
    connect(ui->actionNewPipetune, SIGNAL(triggered()),
            this, SLOT(newTune()));
    connect(ui->actionAbout, SIGNAL(triggered()),
            this, SLOT(about()));
}

void MainWindow::newTune()
{
    if(m_tune != 0){
        delete m_tune;
    }
    m_tune = new GHBTune( scene );
    scene->addItem(m_tune);
    scene->setSceneRect(m_tune->boundingRect());
    view->setSceneRect(0, -50, scene->sceneRect().width()+100, scene->sceneRect().height()+100);
    ui->SymbolToolBar->addActions(m_tune->getActions()->actions());
}

void MainWindow::about()
{
    AboutDialog dialog(this);
    dialog.exec();
}

void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
