/**
 * @file
 * @author  Thomas Baumann <teebaum@ymail.com>
 *
 * @section LICENSE
 *
 * <h3>GNU General Public License version 3</h3>
 * This program is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, see <http://www.gnu.org/licenses/>.
 *
 */
#include "timesignature.h"
#include "pitchlist.h"

#include <QtGui>
#include <QSvgRenderer>
#include <QDebug>

TimeSignature::TimeSignature(const PitchList *pitchList, const QPen *pen)
    :Symbol(pitchList, pen)
{
    m_timeSignatureIds = new QHash<int, QString>;
    m_timeSignatureIds->insert(_2_2, QString("2_2"));
    m_timeSignatureIds->insert(_2_4, QString("2_4"));
    m_timeSignatureIds->insert(_3_4, QString("3_4"));
    m_timeSignatureIds->insert(_4_4, QString("4_4"));
    m_timeSignatureIds->insert(_3_8, QString("3_8"));
    m_timeSignatureIds->insert(_6_8, QString("6_8"));
    m_timeSignatureIds->insert(_9_8, QString("9_8"));
    m_timeSignatureIds->insert(_12_8, QString("12_8"));

    m_svgRenderer = new QSvgRenderer(QString(":/mainwindow/images/time_signatures.svg"), this);

    setTimeSig(_4_4);
}

TimeSignature::~TimeSignature(){
    delete m_svgRenderer;
}

void TimeSignature::setTimeSig(int sig)
{
    if(m_timeSig != sig){
        m_timeSig = sig;
        emit timeSignatureChanged();
    }
    setPreferredSize(getRectForTimeSig().size());
}

void TimeSignature::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    setVisible(true);
    painter->setBrush( QBrush(QColor(Qt::lightGray), Qt::SolidPattern));
    painter->setPen(QColor(Qt::lightGray));
    m_svgRenderer->render(painter, m_timeSignatureIds->value(m_timeSig), getRectForTimeSig());
}

QRectF TimeSignature::boundingRect()
{
    qreal hpw = m_pen->width() / 2;
    QRectF rect = getRectForTimeSig();
    return rect.adjusted( -hpw, -hpw, hpw, hpw );
}

QRectF TimeSignature::getRectForTimeSig()
{
    QRectF t_rect;
    QSizeF t_size = m_svgRenderer->boundsOnElement(m_timeSignatureIds->value(m_timeSig)).size(); //Defaultsize of timesig-svg
    qreal heightToWidth = (qreal)t_size.height() / t_size.width(); //proportion of timesig-svg
    qreal targetHeight = (qreal)m_pitchList->lineHeight() * 4;
    qreal targetWidth = targetHeight / heightToWidth;
    t_rect = QRectF(0, m_pitchList->baseLine(), targetWidth, targetHeight);
    return t_rect;
}
