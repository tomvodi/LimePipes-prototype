/**
 * @file
 * @author  Thomas Baumann <teebaum@ymail.com>
 *
 * @section LICENSE
 *
 * <h3>GNU General Public License version 3</h3>
 * This program is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, see <http://www.gnu.org/licenses/>.
 *
 */
#ifndef CLEF_H
#define CLEF_H

#include "symbol.h"
#include "itemtypes.h"
#include <QHash>

class PitchList;
class QSvgRenderer;

/*! @class Clef
 * @brief This class represents a Clef. It can be a Treble, Bass, Alto and Tenor.
 */
class Clef : public Symbol
{
public:
    enum { Type = ClefType };
    int type() const { return Type; }
    enum clefType{ Treble, Bass, Alto, Tenor };
    Clef(const PitchList *pitchList, const QPen *pen);
    ~Clef();
    void setClef(int clefType);
    int clefType() const {
        return m_clef;
    }
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect();
private:
    /*! The actual clefType */
    int m_clef;
    /*! Every type of Clef has its own svg-file. m_svgRenderer is loaded with
      * that specific file if the clef changes
      */
    QSvgRenderer *m_svgRenderer;
    /*! For every clefType, the associated svg-file is mapped */
    QHash<int, QString> *m_clefs;
    /*! Returns the bounds for the actual clef */
    QRectF getRectForClef() const;
};

#endif // CLEF_H
