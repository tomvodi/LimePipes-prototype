/**
 * @file
 * @author  Thomas Baumann <teebaum@ymail.com>
 *
 * @section LICENSE
 *
 * <h3>GNU General Public License version 3</h3>
 * This program is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, see <http://www.gnu.org/licenses/>.
 *
 */
#include "embellishment.h"
#include "pitch.h"
#include "pitchlist.h"
#include "ghbpitchlist.h"
#include <QtGui>
#include <QSvgRenderer>

#include <QDebug>

Embellishment::Embellishment( const PitchList *pitchList, const QPen *pen )
    :MelodySymbol(pitchList, pen)
{
    m_pitchList = pitchList;
    m_pen = pen;
    m_preceedPitch = 0;
    m_followPitch = 0;
    m_alternative = Embellishment::Regular;
    m_embellishTop = m_pitchList->baseLine()-m_pitchList->lineHeight()*2.5;
    m_graceHeight = pitchList->lineHeight() * 0.5;
    m_contextMenuActions = new QList<QAction *>;

    m_svgRenderer = new QSvgRenderer(QString(":/mainwindow/images/melody_notes_and_flags.svg"), this);
    QString item("melody_fill");
    m_graceRect = getBoundsForId(item, 0.0, m_graceHeight );
    item = "melody_fill_line";
    m_graceWithLineRect = getBoundsForId(item, 0.0, m_graceHeight);
    item = "flag3_up";
    m_flagRect = getBoundsForId(item, m_graceRect.width()*0.8);

    m_graceWidth = m_graceRect.width();
    m_graceSpace = m_graceWidth / 4;
    m_spaceForLine = m_graceWithLineRect.width() - m_graceRect.width();

    m_neighborsOk = false;
    updateBoundingRect();
    /*! @todo Caching is done by every Item itself. Use QPixmapCache. */
    setCacheMode(QGraphicsItem::DeviceCoordinateCache);
    setFlags( QGraphicsItem::ItemIsFocusable |
              QGraphicsItem::ItemIsSelectable);
}

void Embellishment::setFollowPitch(const Pitch *pitch)
{
    if(pitch != 0) {
        if(m_followPitch != 0){
            if(m_followPitch->y() != pitch->y()) {
                m_followPitch = pitch;
            }
        } else {
            m_followPitch = pitch;
        }
    } else {
        m_followPitch = 0;
    }
    update();
    updateEmbellishment();
}

void Embellishment::setPreceedPitch(const Pitch *pitch)
{
    if(pitch != 0){
        if(m_preceedPitch != 0){
            if(m_preceedPitch->y() != pitch->y()) {
                m_preceedPitch = pitch;
            }
        } else {
            m_preceedPitch = pitch;
        }
        //qDebug() <<  "Embellishment->setPreceed: " << m_preceedPitch->name();
    } else {
        m_preceedPitch = 0;
    }
    this->update();
    updateEmbellishment();
}

void Embellishment::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
/*
    //bounding Rect
    painter->setPen( QPen(Qt::green, 1.0) );
    painter->setBrush( Qt::transparent );
    painter->drawRect( boundingRect() );

    //geometry Rect
    painter->setPen( QPen(Qt::darkMagenta, 1.0) );
    painter->setBrush( Qt::transparent );
    painter->drawPolygon( mapFromParent(geometry()) );

    //Shape
    painter->setPen( QPen(Qt::green, 1.0) );
    painter->drawPath( shape() );


    //koordinatensystem
    painter->setPen(QPen(Qt::green, 1.0));
    painter->drawLine(-20, 0, 20, 0);
    painter->drawLine(0, -20, 0, 20);
    painter->setPen(Qt::NoPen);
    //ende koordinatensystem
*/
    qreal graceLeftEdge = m_rect.left();
    QColor color = QColor(Qt::black);
    QString addition;
    if(!m_neighborsOk){
        color = QColor(Qt::red);
        addition = QString("_nok");
    }
    if(hasFocus()){
        color = QColor(Qt::blue);
        addition = QString("_foc");
    }
    if(hovermode()){
        color = QColor(Qt::lightGray);
        addition = QString("_hov");
    }

    painter->setBrush( QBrush(color, Qt::SolidPattern));
    painter->setPen(QPen(QBrush(color, Qt::SolidPattern), 2.0));
    for(int i = 0; i < m_pitches.size(); i++){
        paintGrace(painter, graceLeftEdge, m_pitches[i], addition);
        qreal offset = 0;
        if(hasLineThroughHead(m_pitches[i])){
            offset += m_spaceForLine;
        }
        graceLeftEdge += m_graceWidth + m_graceSpace + offset;
    }
    paintStems(painter);
}

void Embellishment::paintGrace(QPainter *painter, qreal leftEdge, qreal yPos, QString &addition)
{
    QPointF lineStart = QPointF(leftEdge+m_graceWidth-1.5, yPos-m_graceHeight*0.3);
    //Draw Gracehead
    if(hasLineThroughHead(yPos)){
        m_graceWithLineRect.moveTopLeft(QPointF(leftEdge, yPos-m_graceWithLineRect.height()/2));
        m_svgRenderer->render(painter, "melody_fill_line" + addition, m_graceWithLineRect);
        lineStart.setX(lineStart.x()+m_spaceForLine/2);
    } else {
        m_graceRect.moveTopLeft(QPointF(leftEdge, yPos-m_graceRect.height()/2));
        m_svgRenderer->render(painter, "melody_fill" + addition, m_graceRect);
    }

    //Draw Stem
    QPointF lineEnd(lineStart);
    if(m_pitches.count() == 1){
        //lineEnd.setY( lineEnd.y()-m_singleGraceStemLength);
        m_flagRect.moveBottomLeft(QPointF(lineStart.x(), yPos-m_pitchList->lineHeight()/2.5));
        m_svgRenderer->render(painter, "flag3_up" + addition, m_flagRect);
        lineEnd.setY( m_flagRect.top()-1 );
    } else {
        lineEnd.setY( m_embellishTop );
    }
    painter->drawLine(lineStart, lineEnd);
}

void Embellishment::paintStems(QPainter *painter)
{
    qreal leftEdge = m_rect.left()+m_graceWidth;
    qreal rightEdge = m_rect.right()-3;
    painter->save();

    QPen pen = painter->pen();
    pen.setWidth(4.0);
    painter->setPen(pen);
    painter->setRenderHint(QPainter::Antialiasing, false);
    if(hasLineThroughHead(m_pitches.value(0))){
        leftEdge += m_spaceForLine/2;
    }
    if(hasLineThroughHead(m_pitches.value(m_pitches.count()-1))){
        rightEdge -= m_spaceForLine/2;
    }
    qreal topLine = m_embellishTop;
    if(m_pitches.count() != 1){
        painter->drawLine(leftEdge, topLine,
                          rightEdge, topLine);
        painter->drawLine(leftEdge, topLine+m_pitchList->lineHeight()*0.4,
                          rightEdge, topLine+m_pitchList->lineHeight()*0.4);
        painter->drawLine(leftEdge, topLine+m_pitchList->lineHeight()*0.8,
                          rightEdge, topLine+m_pitchList->lineHeight()*0.8);
    }
    painter->restore();
}

void Embellishment::updateBoundingRect()
{
    int gracesCnt = m_pitches.count();
    qreal width = gracesCnt * m_graceWidth + (gracesCnt - 1) * m_graceSpace;
    int numberOfLines = 0;  //Number of gracenotes with line through head
    foreach(qreal pitch, m_pitches){
        if(hasLineThroughHead(pitch)){
            width += m_spaceForLine;
            numberOfLines++;
        }
    }
    if( width <= 0 ) { width = 1; };
    m_rect = QRectF(0, m_pitchList->baseLine()-4.1*m_pitchList->lineHeight(),
                    width, 7.6*m_pitchList->lineHeight());
    if(gracesCnt == 1){
        m_rect.adjust(0.0, 0.0, m_flagRect.width(), 0.0);
    }
    setMinimumWidth(m_rect.width());
    setPreferredWidth(m_rect.width());
    setMaximumWidth(m_rect.width());
}

QRectF Embellishment::boundingRect() const
{
    qreal hpw = m_pen->width() / 2;
    return m_rect.adjusted( -hpw, -hpw, hpw, hpw );
}

bool Embellishment::hasLineThroughHead(qreal y) const
{
    if(y == m_pitchList->getPitch(GHBPitchList::HighA)->y()){
        return true;
    }
    return false;
}

void Embellishment::changePitchesCountTo(int cnt)
{
    int t_cnt = m_pitches.count();
    if(t_cnt == cnt){
        return;
    }
    if(t_cnt < cnt){
        int diff = cnt - t_cnt;
        for(int i = 0; i<diff; i++){
            m_pitches.push_back(m_pitchList->getPitch(GHBPitchList::LowA)->y());
        }
    }
    if(t_cnt > cnt){
        int diff = t_cnt - cnt;
        for(int i = 0; i<diff; i++){
            m_pitches.pop_back();
        }
    }
}

void Embellishment::setPitch(const Pitch *pitch)
{
    Symbol::setPitch(pitch);
    this->updateEmbellishment();
}

void Embellishment::debugPitches() const
{
    qDebug() << "embellishment pitches: ";
    foreach(qreal pos, m_pitches){
        qDebug() << "- " << pos;
    }
}

void Embellishment::mousePressEvent( QGraphicsSceneMouseEvent *event )
{
    if(hovermode()){
        event->ignore();
        return;
    }
    setFocus(Qt::MouseFocusReason);
    update();
}

void Embellishment::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    if(hovermode()){
        event->ignore();
        return;
    }
}

void Embellishment::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if(hovermode()){
        event->ignore();
        return;
    }
}

void Embellishment::keyPressEvent(QKeyEvent *event)
{
    event->ignore();
}

QRectF Embellishment::getBoundsForId(QString &id, qreal targetWidth, qreal targetHeight)
{
    QRectF t_rect;
    qreal t_targetWidth;
    qreal t_targetHeight;
    QSizeF t_size = m_svgRenderer->boundsOnElement(id).size(); //Defaultsize of id in svg
    qreal heightToWidth = (qreal)t_size.height() / t_size.width(); //proportion of id in svg
    if(targetWidth){
        t_targetWidth = targetWidth;
        t_targetHeight = targetWidth * heightToWidth;
    }
    if(targetHeight){
        t_targetHeight = targetHeight;
        t_targetWidth = targetHeight / heightToWidth;

    }
    t_rect = QRectF(0, m_pitchList->baseLine(), t_targetWidth, t_targetHeight);
    return t_rect;
}

void Embellishment::focusOutEvent(QFocusEvent *event)
{
    update();
}

void Embellishment::updateAlternativeFromMenu(QAction *action)
{
    setAlternative(action->data().toInt());
}

void Embellishment::setAlternative(int type)
{
    m_alternative = type;
    updateEmbellishment();
}

void Embellishment::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    if(m_alternatives.count() > 1){
        createActions();
        QMenu menu;
        QActionGroup alternateGroup(this);
        if(m_contextMenuActions->count()) {
            for(int i=0; i<m_contextMenuActions->count(); i++){
                QAction *action = m_contextMenuActions->at(i);
                if(m_alternative == action->data().toInt()){
                    action->setChecked(true);
                }
                alternateGroup.addAction(action);
            }
        }
        menu.addActions(alternateGroup.actions());
        connect(&menu, SIGNAL(triggered(QAction*)),
                this, SLOT(updateAlternativeFromMenu(QAction*)));
        menu.exec(event->screenPos());
    }
}

void Embellishment::createActions()
{
    if(m_alternatives.count() && m_contextMenuActions->count() == 0){
        QAction *action;
        QMapIterator<int, QString> i(m_alternatives);
        while (i.hasNext()) {
            i.next();
            action = new QAction(i.value(), this);
            action->setData(i.key());
            action->setCheckable(true);
            m_contextMenuActions->append(action);
        }
    }
}
