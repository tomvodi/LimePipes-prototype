/**
 * @file
 * @author  Thomas Baumann <teebaum@ymail.com>
 *
 * @section LICENSE
 *
 * <h3>GNU General Public License version 3</h3>
 * This program is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, see <http://www.gnu.org/licenses/>.
 *
 */
#include "singlegrace.h"
#include "pitchlist.h"
#include "ghbpitchlist.h"
#include "pitch.h"

#include <QtGui>

#include "qdebug.h"

SingleGrace::SingleGrace(const PitchList *pitchList, const QPen *pen, const Pitch *pitch)
    :Embellishment(pitchList, pen)
{
    m_pitch = pitch;
    if(m_pitch == 0) {
        m_pitch = m_pitchList->getPitch(GHBPitchList::HighG);
        m_pitches.append( m_pitch->y());
    } else {
        m_pitches.append(m_pitch->y());
    }
    updateEmbellishment();
    m_neighborsOk = true;
}

void SingleGrace::updateEmbellishment()
{
    if(m_pitch->y() != m_pitches[0]){
        m_pitches.replace(0, m_pitch->y());
        update(boundingRect());
    }
    if((m_followPitch != 0) &&
       (m_preceedPitch != 0)){
        if( !isPreceedPitchValid()){
            m_neighborsOk = false;
            //! @todo This message should be processed by the message class http://dev.limepipes.org/issues/5
            setToolTip("Single grace can't be played between this two melody-notes");
        } else {
            m_neighborsOk = true;
        }
    } else if (m_preceedPitch == 0) {
        m_neighborsOk = false;
        setToolTip("Single grace has no preceeding melodynote.");
    } else if(m_followPitch == 0){
        m_neighborsOk = false;
        setToolTip("Single grace has no following melodynote.");
    }
    if(m_neighborsOk){
        setToolTip(false);
    }
    Embellishment::updateBoundingRect();
}

bool SingleGrace::isPreceedPitchValid()
{
    if(!isStrike() && !isSlur()){
        if((m_preceedPitch->y() <= m_pitch->y()) ||
           (m_followPitch->y() <= m_pitch->y())){
            return false;
        }
    }
    return true;
}

bool SingleGrace::isStrike()
{
    if((m_preceedPitch->y() < m_pitch->y()) &&
       (m_preceedPitch->y() == m_followPitch->y())) {
        return true;
    }
    return false;
}

bool SingleGrace::isSlur()
{
    if((m_preceedPitch->y() < m_pitch->y()) &&
       m_followPitch->y() == m_pitch->nextHigher()->y()){
        return true;
    }
    return false;
}

void SingleGrace::mousePressEvent( QGraphicsSceneMouseEvent *event )
{
    Embellishment::mousePressEvent(event);
    m_dragStartY = event->pos().y();
}

void SingleGrace::mouseReleaseEvent( QGraphicsSceneMouseEvent *event )
{
    Embellishment::mouseReleaseEvent(event);
}

void SingleGrace::mouseMoveEvent( QGraphicsSceneMouseEvent *event )
{
    Embellishment::mouseMoveEvent(event);
    qreal yPos = event->pos().y();
    qreal ydist = m_dragStartY - yPos;
    qreal nextPitchDist = m_pitch->lineHeight() / 2;

    //up and down -- Pitch
    if( ydist > 0 ) //up
    {
        if( ydist > (nextPitchDist/2) ){
            if(m_pitch->nextHigher() != 0){
                m_dragStartY -= nextPitchDist;
                setPitch( m_pitch->nextHigher() );
                updateEmbellishment();
            }
        }
    } else if( ydist < 0 ) { //down
        if( -ydist > (nextPitchDist/2) ){
            if(m_pitch->nextLower() != 0){
                m_dragStartY += nextPitchDist;
                setPitch( m_pitch->nextLower() );
                updateEmbellishment();
            }
        }
    }
}
