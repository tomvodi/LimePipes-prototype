/**
 * @file
 * @author  Thomas Baumann <teebaum@ymail.com>
 *
 * @section LICENSE
 *
 * <h3>GNU General Public License version 3</h3>
 * This program is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, see <http://www.gnu.org/licenses/>.
 *
 */
#include "musicbar.h"
#include <QtGui>
#include <QDebug>

#include "pitchlist.h"
#include "standardstemdrawer.h"
#include "symbol.h"
#include "pitch.h"
#include "melodynote.h"
#include "embellishment.h"
#include "clef.h"

#include "ghbpitchlist.h"
#include "singlegrace.h"
#include "doubling.h"
#include "grip.h"

#include <QGraphicsSvgItem>
#include <QSvgRenderer>

MusicBar::MusicBar( QGraphicsScene *scene, const PitchList *pitchList, const Symbol *cursor, const QPen *pen)
    : QGraphicsWidget()
{
    setVisible(false);
    m_scene = scene;
    m_pitchList = pitchList;
    m_margins = 60;
    m_spaceForNonLayoutSym = 10;
    m_cursor = cursor;
    m_cursorIdx = -1;
    m_insertSymbol = 0;
    m_lastSymPrev = 0;
    m_firstSymNext = 0;
    m_rect = QRectF(0,0, geometry().width(), geometry().height());

    m_symbolLayout = new QGraphicsLinearLayout(this);
    m_symbolLayout->setSpacing(4.0); /*! Space between Symbols */

    m_stemDrawer = new StandardStemDrawer(scene, &m_symbols, pen);
    m_stemDrawer->setParentItem(this);

    setLayout(m_symbolLayout);
    setInsertSymbolFromCursor();

    m_clef = new Clef(pitchList, pen);
    m_clef->setX(m_spaceForNonLayoutSym);  //Clef is the most left item
    scene->addItem(m_clef);
    m_clef->setParentItem(this);

    m_timeSig = new TimeSignature(pitchList, pen);
    scene->addItem(m_timeSig);
    m_timeSig->setParentItem(this);

    m_tuneTimeSig = 0;
    m_timeSigDiffers = false;

    setClefVisible(false);
    setTimeSigVisible(false);

    qreal barHeight = 4*m_pitchList->lineHeight();
    setMinimumSize(50, barHeight+2*m_margins);
    setMaximumSize(500, barHeight+2*m_margins);

    setUpbeat(false);

    if( pen == 0)
    {
        m_pen = new QPen();
    } else {
        m_pen = pen;
    }

    setAcceptHoverEvents(true);
    setCacheMode(QGraphicsItem::DeviceCoordinateCache);
    setFlags(QGraphicsItem::ItemIsFocusable);
    createActions();
    createConnections();
}

void MusicBar::paint( QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget )
{
    QPen pen;

#ifdef QT_DEBUG
    painter->setBrush( Qt::transparent );
/*
    //bounding Rect
    painter->setPen( QPen(Qt::blue, 1.0) );
    painter->setBrush( Qt::transparent );
    painter->drawRect( m_rect );
*//*
    //geometry Rect
    painter->setPen( QPen(Qt::darkMagenta, 1.0) );
    painter->setBrush( Qt::transparent );
    painter->drawPolygon( mapFromParent(geometry()) );

    //Shape
    painter->setPen( QPen(Qt::green, 1.0) );
    painter->drawPath( shape() );


    //koordinatensystem
    painter->setPen(QPen(Qt::green, 1.0));
    painter->drawLine(-20, 0, 20, 0);
    painter->drawLine(0, -20, 0, 20);
    painter->setPen(Qt::NoPen);
    //ende koordinatensystem
*/
#endif

    //Bar
    pen = QPen(Qt::black, m_pen->widthF() );
    painter->setBrush( Qt::transparent );
    painter->setPen( pen );
    painter->setRenderHint( QPainter::Antialiasing, false );
    //Draw horizontal lines
    for( int i = 0; i<5; i++ ) {
        painter->drawLine(m_rect.left(), m_margins + i * m_pitchList->lineHeight(),
                          m_rect.right(), m_margins + i * m_pitchList->lineHeight());
    }
    //Draw vertical line on right edge
    pen.setCosmetic(true);
    painter->setRenderHint( QPainter::Antialiasing, true );  //It won't work without anitaliasing
    painter->drawLine(m_rect.right(), m_margins+1, //+1 for compensate antialiasing
                      m_rect.right(), m_margins + 4* m_pitchList->lineHeight());
}

QRectF MusicBar::boundingRect() const
{
    const double hpw = m_pen->widthF()/2;
    return m_rect.adjusted( -hpw, -hpw, hpw, hpw );
}

void MusicBar::setPen(const QPen *pen)
{
    m_pen = pen;
}

void MusicBar::insert(int idx, Symbol *symbol)
{
    int insert_idx = idx;
    if(insert_idx != 0 && insert_idx > m_symbols.count()-1){
        insert_idx = m_symbols.count()+1;
    }
    symbol->setVisible(true);
    m_symbols.insert(insert_idx, symbol);
    if(symbol->scene() == 0){
        m_scene->addItem(symbol);
    }
    updateLayout();

    Symbol *symBefore = 0;
    Symbol *symAfter = 0;
    //Symbol not first or last
    if(idx > 0){
        symBefore = m_symbols[idx-1];
    }
    if(idx < m_symbols.count()-1){
        symAfter = m_symbols[idx+1];
    }
    //Symbol inserted as first
    if(idx == 0){
        symBefore = m_lastSymPrev;
    }
    //Symbol inserted as last
    if(idx == m_symbols.count()-1){
        symAfter = m_firstSymNext;
    }
    if(symbol->type() == MelodyNoteType)
    {
        MelodyNote *note = qgraphicsitem_cast<MelodyNote *>(symbol);
        connect(note, SIGNAL(pitchHasChanged(const Pitch *)),
                m_stemDrawer, SLOT(updateStems()));
        connect(note, SIGNAL(lengthHasChanged()),
                m_stemDrawer, SLOT(updateStems()));
    }
    connectSymbols(symBefore, symbol);
    connectSymbols(symbol, symAfter);
}

void MusicBar::removeSymbol(int idx)
{
    Symbol *left = 0;
    Symbol *right = 0;
    //Symbol not first or last
    if(idx > 0){
        left = m_symbols.at(idx-1);
        left->disconnect(m_symbols.at(idx));
    }
    if(idx < m_symbols.count()-1){
        right = m_symbols.at(idx+1);
        right->disconnect(m_symbols.at(idx));
    }
    //Symbol removed is first
    if(idx == 0 && m_lastSymPrev != 0){
        left = m_lastSymPrev;
        m_lastSymPrev->disconnect(m_symbols.at(idx));
    }
    //Symbol removed is last
    if(idx == m_symbols.count()-1 && m_firstSymNext != 0){
        right = m_firstSymNext;
        m_firstSymNext->disconnect(m_symbols.at(idx));
    }
    m_symbols.removeAt(idx);
    connectSymbols(left, right);
}

void MusicBar::connectSymbols(Symbol *left, Symbol *right)
{
    if(left != 0 && left->inherits("Embellishment")){
        Embellishment *emb = qobject_cast<Embellishment *>(left);
        if(right != 0 && right->type() == MelodyNoteType){
            //qDebug() <<  "left Embellishment - right Melody";
            MelodyNote *note = qgraphicsitem_cast<MelodyNote *>(right);
            emb->setFollowPitch(note->pitch());
            connect(note, SIGNAL(pitchHasChanged(const Pitch*)),
                    emb, SLOT(setFollowPitch(const Pitch*)));
        } else {
            emb->setFollowPitch(0);
        }
    }
    if(right != 0 && right->inherits("Embellishment")){
        Embellishment *emb = qobject_cast<Embellishment *>(right);
        if(left != 0 && left->type() == MelodyNoteType){
            //qDebug() << "left Melody - right Embellishment";
            MelodyNote *note = qgraphicsitem_cast<MelodyNote *>(left);
            emb->setPreceedPitch(note->pitch());
            connect(note, SIGNAL(pitchHasChanged(const Pitch*)),
                    emb, SLOT(setPreceedPitch(const Pitch*)));
        } else {
            emb->setPreceedPitch(0);
        }
    }
}

void MusicBar::updateLayout()
{
    int lay_cnt = m_symbolLayout->count();
    for(int y=0; y<lay_cnt; y++){
        m_symbolLayout->removeAt(0);
    }
    for(int y=0; y<m_symbols.count(); y++){
        m_symbolLayout->addItem(m_symbols.at(y));
    }
    m_symbolLayout->setGeometry(m_rect);
}

void MusicBar::append( Symbol *symbol)
{
    symbol->setVisible(true);

    Symbol *lastSym = 0;
    if( !m_symbols.isEmpty() ){
        lastSym = m_symbols.last();
    }

    m_symbols.append( symbol );
    m_symbolLayout->addItem( symbol );

    if(symbol->type() == MelodyNoteType)
    {
        MelodyNote *note = qgraphicsitem_cast<MelodyNote *>(symbol);
        connect(note, SIGNAL(pitchHasChanged(const Pitch *)),
                m_stemDrawer, SLOT(updateStems()));
        connect(note, SIGNAL(lengthHasChanged()),
                m_stemDrawer, SLOT(updateStems()));
        if((lastSym != 0) && (lastSym->inherits("Embellishment")) ){
            Embellishment *emb = qobject_cast<Embellishment *>(lastSym);
            emb->setFollowPitch(note->pitch());
            connect(note, SIGNAL(pitchHasChanged(const Pitch*)),
                    emb, SLOT(setFollowPitch(const Pitch *)));
        }
    } else if (symbol->inherits("Embellishment")) {
        if((lastSym != 0) && (lastSym->type() == MelodyNoteType)){
            MelodyNote *note = qgraphicsitem_cast<MelodyNote *>(lastSym);
            Embellishment *emb = qobject_cast<Embellishment *>(symbol);
            emb->setPreceedPitch(note->pitch());
            connect(note, SIGNAL(pitchHasChanged(const Pitch*)),
                    emb, SLOT(setPreceedPitch(const Pitch*)));
        }
    }
}

void MusicBar::append(QList<Symbol *>symbols)
{
    QList<Symbol *>::const_iterator i;
    for( i = symbols.begin(); i != symbols.end(); i++ )
    {
        (*i)->setParentItem(this);
        m_symbolLayout->addItem(*i);
        parentItem()->update();
    }
}

void MusicBar::prepend(Symbol *symbol)
{
    symbol->setParentItem(this);
    m_symbols.prepend(symbol);
}

void MusicBar::prepend(QList<Symbol *>symbols)
{
    QList<Symbol *>::const_iterator i;
    for( i = symbols.end(); i != symbols.begin(); i-- )
    {
        (*i)->setParentItem(this);
        m_symbols.prepend( *i );
    }
}

void MusicBar::setGeometry(const QRectF &rect)
{
//    qDebug() << "----------------Set geometry musba: ";
//    qDebug() << "Layout contentsrecht: " << m_symbolLayout->contentsRect().width();
//    qDebug() << "Layout geometry: " << m_symbolLayout->geometry();
//    qDebug() << "geometry: " << rect;
//    qDebug() << "contents rect: " << contentsRect();
//    qDebug() << "preferred size: " << preferredSize();
//    qDebug() << "--------------------------------";
    if(m_symbolLayout->contentsRect().width() > rect.width()){
        setGeometry(QRectF(rect.left(), rect.top(), m_symbolLayout->geometry().width(), rect.height()));
    }
    QGraphicsWidget::setGeometry(rect);
    setVisible(true);
    m_rect.setWidth(rect.width());
    m_rect.setHeight(rect.height());
}

void MusicBar::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    if(m_cursor == 0){
        return;
    }
    if(!spaceForSymbol(m_insertSymbol)){
        return;
    }
    //Symbols in bar, no insert symbol is shown and cursor ahead of last symbol
    if(m_symbols.count() != 0 &&
       m_cursorIdx == -1 &&
       m_symbols.last()->geometry().right() <= event->pos().x()){
            insert(m_symbols.count(), m_insertSymbol);
            m_cursorIdx = m_symbols.count()-1;
    } else if(m_symbols.count() == 0){ //no Symbols yet in bar
        insert(0, m_insertSymbol);
        m_cursorIdx = 0;
    }
}

void MusicBar::hoverMoveEvent(QGraphicsSceneHoverEvent *event)
{
    if(m_cursor == 0){
        return;
    }
    if(!spaceForSymbol(m_insertSymbol)){
        return;
    }
    if(m_insertSymbol->hasPitch()){
        m_insertSymbol->setPitch(m_pitchList->pitchForPos(event->pos().y()));
        m_insertSymbol->update();
        m_stemDrawer->updateStems();
    }
    QGraphicsLayoutItem *t_item = 0;
    for(int i=0; i<m_symbolLayout->count(); i++)
    {
        t_item = m_symbolLayout->itemAt(i);
        if((t_item->geometry().x() <= event->pos().x()) &&
           (event->pos().x() <= t_item->geometry().x()+ t_item->geometry().width()) ){
            if(m_cursorIdx == -1){ //no gap yet
                m_cursorIdx = i;
                insert(i, m_insertSymbol);
            }
            else { //gap already there ...
                if(i != m_cursorIdx){
                    removeSymbol(m_cursorIdx);
                    m_insertSymbol->disconnect();
                    insert(i, m_insertSymbol);
                    m_cursorIdx = i;
                    update(m_rect);
                }
            }
        }
    }
    //Symbols in bar, no insert symbol is shown and cursor ahead of last symbol
    if(m_symbols.count() != 0 &&
       m_cursorIdx == -1 &&
       m_symbols.last()->geometry().right() <= event->pos().x()){
        insert(m_symbols.count(), m_insertSymbol);
        m_cursorIdx = m_symbols.count()-1;
    }
}

void MusicBar::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
    if(m_cursor == 0){
        return;
    }
    if(m_cursorIdx != -1){
        removeSymbol(m_cursorIdx);
        updateLayout();
        m_cursorIdx = -1;
        m_insertSymbol->setVisible(false);
        m_stemDrawer->update(m_stemDrawer->boundingRect());
    }
    m_symbolLayout->setGeometry(m_rect);
}

void MusicBar::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    setFocus(Qt::MouseFocusReason);
    if(m_cursor == 0){
        event->ignore();
        return;
    }
}

void MusicBar::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if(m_cursor == 0){
        return;
    }
    m_insertSymbol->setHovermode(false);
    m_insertSymbol->update();
    if(m_cursorIdx == m_symbols.count()-1){
        emit lastSymChanged(m_insertSymbol);
    }
    if(m_cursorIdx == 0){
        emit firstSymChanged(m_insertSymbol);
    }
    setInsertSymbolFromCursor();
    m_cursorIdx = -1;
}

void MusicBar::setInsertSymbolFromCursor()
{
    /*! @todo There has to be something to get all Embellishments, or else
      * this code has to be modified for every new Embellishmet
      */
    int t_insSymType = 0;
    if(m_insertSymbol != 0){
        t_insSymType = m_insertSymbol->type();
    }
    if(m_cursor != 0){
        int type = m_cursor->type();
        switch(type){
            case MelodyNoteType: {
                    const MelodyNote *note = qgraphicsitem_cast<const MelodyNote *>(m_cursor);
                    m_insertSymbol = new MelodyNote(note->pitchList(), note->pen(), note->pitch(), note->length()->length());
                    m_insertSymbol->setVisible(true);
                break;
            }
            case DoublingType: {
                    const Doubling *dbl = qgraphicsitem_cast<const Doubling *>(m_cursor);
                    m_insertSymbol = new Doubling(dbl->pitchList(), dbl->pen());
                break;
            }
            case SingleGraceType: {
                    const SingleGrace *grace = qgraphicsitem_cast<const SingleGrace *>(m_cursor);
                    m_insertSymbol = new SingleGrace(grace->pitchList(), grace->pen());
                break;
            }
        case GripType: {
                const Grip *grip = qgraphicsitem_cast<const Grip *>(m_cursor);
                m_insertSymbol = new Grip(grip->pitchList(), grip->pen());
            }
        }
        m_insertSymbol->setHovermode(true);
        m_insertSymbol->setFlags(QGraphicsItem::ItemIsFocusable);
        m_insertSymbol->setFocus(Qt::MouseFocusReason);
    } else {
        if(m_cursorIdx != -1){
            removeSymbol(m_cursorIdx);
            updateLayout();
            m_cursorIdx = -1;
            m_stemDrawer->update(m_stemDrawer->boundingRect());
        }
        if(m_insertSymbol != 0){
            delete m_insertSymbol;
            m_insertSymbol = 0;
        }
    }
}

void MusicBar::symbolsDebug()
{
    Symbol *sym;
    qDebug() << "----Symbols Debug----";
    qDebug() << "m_symbols:";
    for( int i=0; i<m_symbols.count(); i++){
        sym = m_symbols.at(i);
        if(sym->type() == MelodyNoteType){
            qDebug() << "- " << qgraphicsitem_cast<const MelodyNote *>(sym)->pitch()->name();
        }
        if(sym->type() == SingleGraceType){
            qDebug() << "- SingleGrace: " << qgraphicsitem_cast<const SingleGrace *>(sym)->pitch()->name();
        }
        if(sym->type() == DoublingType){
            qDebug() << "- Doubling";
            //qgraphicsitem_cast<const Doubling *>(sym)->debugPitches();
        }
    }
    qDebug() << "layout cnt: " << m_symbolLayout->count();
    qDebug() << "insert idx: " << m_cursorIdx;
    qDebug() << "----Symbols Debug----";
}

void MusicBar::updateCursor(const Symbol *new_cursor)
{
    m_cursor = new_cursor;
    setInsertSymbolFromCursor();
}

void MusicBar::setClef(int type)
{
    if(m_clef->clefType() != type){
        m_clef->setClef(type);
    }
}

void MusicBar::setClefVisible(bool visible){
    m_clefVisible = visible;
    if(m_clefVisible){
        m_clef->setVisible(true);
    } else {
        m_clef->setVisible(false);
    }
    updateLayoutMargins();
}

void MusicBar::setTimeSig(int type)
{
    if(m_timeSig->timeSigType() != type){
        m_timeSig->setTimeSig(type);
        if(m_timeSig->timeSigType() != m_tuneTimeSig->timeSigType()){
            m_timeSigDiffers = true;
            setTimeSigVisible(true);
        } else {
            m_timeSigDiffers = false;
            setTimeSigVisible(false);
        }
        updateLayoutMargins();
    }
}

void MusicBar::setTuneTimeSig(const TimeSignature *timeSig)
{
    if(m_tuneTimeSig == 0){
        m_tuneTimeSig = timeSig;
        m_timeSig->setTimeSig(timeSig->timeSigType());
        m_timeSigDiffers = false;
        connect(m_tuneTimeSig, SIGNAL(timeSignatureChanged()),
                this, SLOT(updateTimeSignature()));
    }
}

void MusicBar::updateTimeSignature()
{
    if(!m_timeSigDiffers){
        m_timeSig->setTimeSig(m_tuneTimeSig->timeSigType());
    } else {
        if(m_timeSig->timeSigType() == m_tuneTimeSig->timeSigType()){
            m_timeSigDiffers = false;
            setTimeSigVisible(false);
        }
    }
    updateLayoutMargins();
}

void MusicBar::setTimeSigVisible(bool visible){
    m_timeSigVisible = visible;
    if(m_timeSigVisible){
        m_timeSig->setVisible(true);
    } else {
        m_timeSig->setVisible(false);
    }
    updateLayoutMargins();
}

void MusicBar::updateLayoutMargins(){
    qreal leftPos = 0.0;
    int spacesToAdd = 0;
    if(m_clefVisible){
        leftPos += m_clef->boundingRect().width();
        spacesToAdd++;
        m_clef->setVisible(true);
    } else {
        m_clef->setVisible(false);
    }
    if(m_timeSigVisible){
        leftPos += m_timeSig->boundingRect().width();
        spacesToAdd++;
        m_timeSig->setVisible(true);
    } else {
        m_timeSig->setVisible(false);
    }
        spacesToAdd++;  //Add some space after Symbol(s)

    leftPos += spacesToAdd * m_spaceForNonLayoutSym;
    m_symbolLayout->setContentsMargins(leftPos, 0.0, 5.0, 0.0);

    setNonLayoutSymbolPositions();
}

void MusicBar::setNonLayoutSymbolPositions(){
    if(m_clefVisible){
        m_timeSig->setX(2* m_spaceForNonLayoutSym + m_clef->boundingRect().width());
    } else {
        m_timeSig->setX(m_spaceForNonLayoutSym);
    }
}

bool MusicBar::spaceForSymbol(const Symbol *sym){
    /*! @todo This function doesn't recognize:
      * - if some of the symbols change their width (e.g.melodynote changed to highA)
      * - if musicbar shows clef or timesignature and the layout has set contentsmargins,
      *   the symbols can go over the right edge of musicbar
      */
    if( m_symbolLayout->count() &&
        (m_symbolLayout->contentsRect().width() + 2*sym->boundingRect().width() > m_rect.width()) ){
        return false;
    }
    return true;
}

void MusicBar::setUpbeat(bool upbeat)
{
    if(m_isUpbeat != upbeat){
        m_isUpbeat = upbeat;
    }
    if(m_isUpbeat){
        setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Fixed);

    } else {
        setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    }
}

void MusicBar::keyPressEvent(QKeyEvent *event)
{
    //qDebug() << "MusicBar Key press event";
    if(event->key() == Qt::Key_Delete){
        for(int i=0; i<m_symbols.size(); i++){
            Symbol *t_sym = m_symbols.at(i);
            if(t_sym->hasFocus()){
                removeSymbol(i);
                delete t_sym;
            }
        }
        updateLayout();
    } else {
        event->ignore();
    }
}

void MusicBar::setLastSymOfPrev(Symbol *symbol)
{
    //qDebug() << "Set last Symbol of previous Bar";
    m_lastSymPrev = symbol;
    if(m_symbols.count()){
        connectSymbols(m_lastSymPrev, m_symbols.at(0));
    }
}

void MusicBar::setFirstSymOfNext(Symbol *symbol)
{
    //qDebug() << "Set first Symbol of next Bar";
    m_firstSymNext = symbol;
    if(m_symbols.count()){
        connectSymbols(m_symbols.at(m_symbols.count()-1), m_firstSymNext);
    }
}

void MusicBar::createActions()
{
    timeSig22Action = new QAction("2/2", this);
    timeSig22Action->setData(TimeSignature::_2_2);
    timeSig24Action = new QAction("2/4", this);
    timeSig24Action->setData(TimeSignature::_2_4);
    timeSig34Action = new QAction("3/4", this);
    timeSig34Action->setData(TimeSignature::_3_4);
    timeSig44Action = new QAction("4/4", this);
    timeSig44Action->setData(TimeSignature::_4_4);
    timeSig38Action = new QAction("3/8", this);
    timeSig38Action->setData(TimeSignature::_3_8);
    timeSig68Action = new QAction("6/8", this);
    timeSig68Action->setData(TimeSignature::_6_8);
    timeSig98Action = new QAction("9/8", this);
    timeSig98Action->setData(TimeSignature::_9_8);
    timeSig128Action = new QAction("12/8", this);
    timeSig128Action->setData(TimeSignature::_12_8);
    timeSigVisibleAction = new QAction("Timesignature visible", this);
    timeSigVisibleAction->setCheckable(true);
}

void MusicBar::createConnections()
{
    connect(timeSigVisibleAction, SIGNAL(triggered(bool)),
            this, SLOT(setTimeSigVisible(bool)));
}

void MusicBar::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    QMenu menu;
    QMenu timeSigMenu("Set Timesignature");
    timeSigMenu.setToolTip(tr("Change the timesignature for this Bar"));
    timeSigMenu.addAction(timeSig22Action);
    timeSigMenu.addAction(timeSig24Action);
    timeSigMenu.addAction(timeSig34Action);
    timeSigMenu.addAction(timeSig44Action);
    timeSigMenu.addAction(timeSig38Action);
    timeSigMenu.addAction(timeSig68Action);
    timeSigMenu.addAction(timeSig98Action);
    timeSigMenu.addAction(timeSig128Action);
    menu.addMenu(&timeSigMenu);
    connect(&menu, SIGNAL(triggered(QAction*)),
            this, SLOT(updateTimeSig(QAction*)));
    if(!m_timeSigDiffers){
        timeSigVisibleAction->setChecked(m_timeSigVisible);
        menu.addAction(timeSigVisibleAction);
    }

    menu.exec(event->screenPos());
}

void MusicBar::updateTimeSig(QAction *action)
{
    QRegExp regex("[123469]{1,2}/[248]"); // TimeSignature Action
    if(regex.indexIn(action->text()) != -1 ){
        setTimeSig(action->data().toInt());
    }
}
