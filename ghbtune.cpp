/**
 * @file
 * @author  Thomas Baumann <teebaum@ymail.com>
 *
 * @section LICENSE
 *
 * <h3>GNU General Public License version 3</h3>
 * This program is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, see <http://www.gnu.org/licenses/>.
 *
 */
#include "ghbtune.h"

#include <QGraphicsScene>
#include <QDebug>
#include <QtGui>

#include "ghbpitchlist.h"
#include "symbol.h"
#include "musicbar.h"
#include "melodynote.h"
#include "doubling.h"
#include "singlegrace.h"
#include "grip.h"
#include "timesignature.h"

#include <QGraphicsLinearLayout>

GHBTune::GHBTune( QGraphicsScene *scene)
    :QGraphicsWidget()
{
    createActions();
    createConnections();
    m_scene = scene;
    m_cursor = 0;
    m_margin = 35;
    m_rect = QRectF(0,0, 4*500+4*m_margin, 3000);
    /*! QPen for drawing all items */
    m_pen = new QPen();
    m_pen->setCosmetic(true);
    m_pen->setColor(Qt::black);
    m_pitchList = new GHBPitchList( 20.0 );
    m_timeSig = new TimeSignature(m_pitchList, m_pen);

    int text_pos = 70;
    m_title = new QGraphicsTextItem(this, m_scene);
    m_title->setFont(QFont("Arial", 35, QFont::Bold));
    m_title->setTextInteractionFlags(Qt::TextEditorInteraction);
    m_title->setPlainText("Title");
    m_title->setPos(1000, text_pos);

    m_type = new QGraphicsTextItem(this, m_scene);
    m_type->setFont(QFont("Times New Roman", 20, QFont::Bold));
    m_type->setTextInteractionFlags(Qt::TextEditorInteraction);
    m_type->setPlainText("Type");
    m_type->setPos(100, text_pos+m_title->boundingRect().height()-m_type->boundingRect().height());

    m_composer = new QGraphicsTextItem(this, m_scene);
    m_composer->setFont(QFont("Arial", 20, QFont::Bold, true));
    m_composer->setTextInteractionFlags(Qt::TextEditorInteraction);
    m_composer->setPlainText("Composer");
    m_composer->setPos(1800, text_pos+m_title->boundingRect().height()-m_composer->boundingRect().height());

    setMinimumSize( m_rect.width(), m_rect.height() );
    setPreferredSize(m_rect.width(), m_rect.height());
    setMaximumSize( m_rect.width(), m_rect.height());


    //get baseline
    MusicBar *sym = new MusicBar(m_scene, m_pitchList, m_cursor, m_pen);
    m_pitchList->setBaseLine(sym->baseLine());
    m_pitchList->cachePositions((int)sym->boundingRect().top(), (int)sym->boundingRect().bottom());
    delete sym;

    m_vlayout = new QGraphicsLinearLayout(Qt::Vertical);
    m_vlayout->setSpacing(0.0); /*! No space between Rows */
    m_vlayout->setContentsMargins(0.0, 0.0, 0.0, 0.0 );
    m_vlayout->setContentsMargins(m_margin,180,m_margin,0);
    setLayout(m_vlayout);

    m_actions = new QActionGroup(this);
    m_actions->setExclusive(false);
    connect(m_actions, SIGNAL(triggered(QAction*)),
            this, SLOT(updateActions(QAction*)));
    QString name = "MelodyNote";
    m_cursorSymbols.insert(name, new MelodyNote(m_pitchList, m_pen, m_pitchList->getPitch(GHBPitchList::LowG), NoteLength::Eighth));
    QAction *action = new QAction(QIcon(":/mainwindow/images/MelodyNote.png"), name, m_actions);
    action->setCheckable(true);

    name = "Doubling";
    m_cursorSymbols.insert(name, new Doubling(m_pitchList, m_pen));
    action = new QAction(QIcon(":/mainwindow/images/Doubling.png"), name, m_actions);
    action->setCheckable(true);

    name = "SingleGrace";
    m_cursorSymbols.insert(name, new SingleGrace(m_pitchList, m_pen, m_pitchList->getPitch(GHBPitchList::D)));
    action = new QAction(QIcon(":/mainwindow/images/SingleGrace.png"), name, m_actions);
    action->setCheckable(true);

    name = "Grip";
    m_cursorSymbols.insert(name, new Grip(m_pitchList, m_pen));

    action = new QAction(QIcon(":/mainwindow/images/Grip.png"), name, m_actions);
    action->setCheckable(true);

    QGraphicsLinearLayout *rowLayout = new QGraphicsLinearLayout(Qt::Horizontal, m_vlayout);
    rowLayout->setSpacing(0.0); //! No space between MusicBars
    rowLayout->setContentsMargins(0.0, 0.0, 0.0, 0.0 );
    rowLayout->addStretch(m_margin);

    addPart();

    m_shadow = new QGraphicsDropShadowEffect(this);
    m_shadow->setBlurRadius(15);
    m_shadow->setOffset(0);
    setGraphicsEffect(m_shadow);
    setFlags(QGraphicsItem::ItemIsFocusable);
    setFocus(Qt::ActiveWindowFocusReason);
}

void GHBTune::updateActions(QAction *action)
{
    QString actionText = action->text();
    if(action->isChecked()){
        QAction *t_action;
        foreach(t_action, m_actions->actions()){
            if(actionText != t_action->text()){
                t_action->setChecked(false);
            }
        }
        setCursorForAction(action);
    } else {
        setCursorForAction(0);
    }
}

void GHBTune::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
/*
    qDebug() << "GHBTune: paint";
    qDebug() << "m_rect: " << m_rect;
    qDebug() << "geometry: " << geometry();
    //bounding Rect
    painter->setPen( QPen(Qt::blue, 1.0) );
    painter->setBrush( Qt::transparent );
    painter->drawRect( boundingRect() );

    //geometry Rect
    painter->setPen( QPen(Qt::darkMagenta, 1.0) );
    painter->setBrush( Qt::transparent );
    painter->drawPolygon( mapFromParent(geometry()) );
*/
    setVisible(true);
    QPen pen = QPen(Qt::red, m_pen->widthF() );
    painter->setBrush( QBrush(Qt::white, Qt::SolidPattern) );
    painter->setPen( Qt::NoPen );
    painter->setRenderHint( QPainter::Antialiasing, false );
    painter->drawRect( m_rect );
    painter->setPen( pen );
}

void GHBTune::setGeometry(const QRectF &rect)
{
    QGraphicsWidget::setGeometry(rect);
    m_rect.setWidth(rect.width());
    m_rect.setHeight(rect.height());
}

void GHBTune::debugSlot(bool tf)
{
    qDebug() << "Debug slot " <<  tf;
}

GHBTune::~GHBTune()
{
    delete m_pitchList;
    delete m_actions;
}

const QActionGroup *GHBTune::getActions()
{
    return m_actions;
}

void GHBTune::setCursorForAction(QAction *action)
{
    if(action != 0){
        m_cursor = m_cursorSymbols[action->text()];
    } else {
        m_cursor = 0;
    }
    QVector<MusicBar *>::iterator i;
    for(i=m_bars.begin(); i != m_bars.end(); i++){
        (*i)->updateCursor(m_cursor);
    }
}


QRectF GHBTune::boundingRect() const
{
    const double hpw = m_pen->widthF()/2;
    return m_rect.adjusted( -hpw, -hpw, hpw, hpw );
}


QSizePolicy GHBTune::sizePolicy() const
{
    return QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
}

void GHBTune::keyPressEvent(QKeyEvent *event)
{
    //qDebug() << "GhbTune key press";
    switch(event->key()){
    case Qt::Key_Escape: {
            //qDebug() << "GHBTune Escape pressed";
            QAction *t_action;
            foreach(t_action, m_actions->actions()){
                if(t_action->isChecked()){
                    t_action->setChecked(false);
                    setCursorForAction(0);
                }
            }
            break;
        }
    default: {
            event->ignore();
        }
    }
}

void GHBTune::addPart()
{
    int firstNewIdx = m_bars.count();
    for( int i=0; i<2; i++ )
    {
        QGraphicsLinearLayout *rowLayout = new QGraphicsLinearLayout(Qt::Horizontal, m_vlayout);
        rowLayout->setSpacing(0.0); //! No space between MusicBars
        rowLayout->setContentsMargins(m_margin, 0.0, m_margin, 0.0 );
        for( int y=0; y<4; y++){
            MusicBar *t_bar = new MusicBar(m_scene, m_pitchList, m_cursor, m_pen);
            t_bar->setTuneTimeSig(m_timeSig);
            if(y==0){
                t_bar->setClefVisible(true);
                if(i==0){  //First row, show time signature
                    t_bar->setTimeSigVisible(true);
                }
                if(!(i%2)){ //Every second line => new part, add upbeat
                    t_bar->setUpbeat(true);
                    m_bars.append(t_bar);
                    rowLayout->addItem(t_bar);
                    t_bar = new MusicBar(m_scene, m_pitchList, m_cursor, m_pen);
                    t_bar->setTuneTimeSig(m_timeSig);
                }
            }
            if(i%2){
                t_bar->setPreferredWidth(m_rect.width()/4);
            }
            m_bars.append(t_bar);
            rowLayout->addItem(t_bar);
        }
        m_vlayout->addItem(rowLayout);
    }
    //Connect Bars only in a part
    for(int r=firstNewIdx; r<m_bars.count(); r++){
        MusicBar *thisbar = m_bars.at(r);
        if(r > firstNewIdx){
            MusicBar *prev_bar = m_bars.at(r-1);
            connect(prev_bar, SIGNAL(lastSymChanged(Symbol*)),
                    thisbar, SLOT(setLastSymOfPrev(Symbol *)));
            connect(thisbar, SIGNAL(firstSymChanged(Symbol*)),
                    prev_bar, SLOT(setFirstSymOfNext(Symbol*)));
        }
    }
}

void GHBTune::updateTimeSig(QAction *action)
{
    QRegExp regex("[123469]{1,2}/[248]"); // TimeSignature Action
    if(regex.indexIn(action->text()) != -1 ){
        setTimeSignature(action->data().toInt());
    }
}

void GHBTune::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    setFocus(Qt::MouseFocusReason);
    event->ignore();
}

void GHBTune::hoverMoveEvent(QGraphicsSceneHoverEvent *event)
{
    setFocus(Qt::MouseFocusReason);
    event->ignore();
}

void GHBTune::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    QMenu menu;
    menu.addAction(addPartAction);
    QMenu timeSigMenu("Set Timesignature");
    timeSigMenu.setToolTip(tr("Change the timesignature for the whole tune"));
    timeSigMenu.addAction(timeSig22Action);
    timeSigMenu.addAction(timeSig24Action);
    timeSigMenu.addAction(timeSig34Action);
    timeSigMenu.addAction(timeSig44Action);
    timeSigMenu.addAction(timeSig38Action);
    timeSigMenu.addAction(timeSig68Action);
    timeSigMenu.addAction(timeSig98Action);
    timeSigMenu.addAction(timeSig128Action);
    menu.addMenu(&timeSigMenu);
    connect(&menu, SIGNAL(triggered(QAction*)),
            this, SLOT(updateTimeSig(QAction*)));
    menu.exec(event->screenPos());
}

void GHBTune::createActions()
{
    addPartAction = new QAction("Add Part", this);
    timeSig22Action = new QAction("2/2", this);
    timeSig22Action->setData(TimeSignature::_2_2);
    timeSig24Action = new QAction("2/4", this);
    timeSig24Action->setData(TimeSignature::_2_4);
    timeSig34Action = new QAction("3/4", this);
    timeSig34Action->setData(TimeSignature::_3_4);
    timeSig44Action = new QAction("4/4", this);
    timeSig44Action->setData(TimeSignature::_4_4);
    timeSig38Action = new QAction("3/8", this);
    timeSig38Action->setData(TimeSignature::_3_8);
    timeSig68Action = new QAction("6/8", this);
    timeSig68Action->setData(TimeSignature::_6_8);
    timeSig98Action = new QAction("9/8", this);
    timeSig98Action->setData(TimeSignature::_9_8);
    timeSig128Action = new QAction("12/8", this);
    timeSig128Action->setData(TimeSignature::_12_8);
}

void GHBTune::createConnections()
{
    connect(addPartAction, SIGNAL(triggered()),
            this, SLOT(addPart()));
}

void GHBTune::setTimeSignature(int type)
{
    if(m_timeSig->timeSigType() != type){
        m_timeSig->setTimeSig(type);
    }
}
