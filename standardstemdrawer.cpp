/**
 * @file
 * @author  Thomas Baumann <teebaum@ymail.com>
 *
 * @section LICENSE
 *
 * <h3>GNU General Public License version 3</h3>
 * This program is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, see <http://www.gnu.org/licenses/>.
 *
 */
#include "standardstemdrawer.h"
#include <QtGui>
#include <QSvgRenderer>
#include "symbol.h"
#include "melodynote.h"
#include "notelength.h"
#include <QDebug>


StandardStemDrawer::StandardStemDrawer(QGraphicsScene *scene, const QList<Symbol *> *symbolList, const QPen *pen)
    :StemDrawer(scene, symbolList, pen)
{

}

void StandardStemDrawer::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    if(m_rect.isEmpty()){
        m_rect = QRectF(parentItem()->boundingRect());
    }
/*
    //bounding Rect
    painter->setPen( QPen(Qt::darkBlue, 1.0) );
    painter->drawRect( boundingRect() );

    //Koordinatensystem
    painter->drawLine(0, -20, 0, 20);
    painter->drawLine(-20, 0, 20, 0);
*/
    QList<Symbol *>::const_iterator i;
    for( i = m_symbolList->begin(); i<m_symbolList->end(); i++ )
    {
        if( (*i)->type() ==  MelodyNoteType ){
            MelodyNote *note = qgraphicsitem_cast<MelodyNote *>(*i);
            QColor color = QColor(Qt::black);
            if(note->hasFocus())
                color = QColor(Qt::blue);
            if(note->hovermode())
                color = QColor(Qt::lightGray);
            painter->setPen( QPen(color, 1.0));


            drawStem( painter, note, note->length() );
        }

    }
}

QRectF StandardStemDrawer::boundingRect() const
{
    const double hpw = m_pen->widthF()/2;
    return m_rect.adjusted( -hpw, -hpw, hpw, hpw );
}

void StandardStemDrawer::drawStem(QPainter *painter, const MelodyNote *note, const NoteLength *length)
{
    QPointF t_pointTop = note->leftConnection();
    t_pointTop += QPoint(m_pen->widthF(), 0.0);
    t_pointTop = mapFromParent(t_pointTop);
    QPointF t_pointBottom = QPoint();
    t_pointBottom.setX(t_pointTop.x());

    QString addition = getImageAddition(note);
    QRectF flagRect = QRectF();

    switch(length->length()){
    case NoteLength::Whole:
        break;
    case NoteLength::Half:
    case NoteLength::Quarter:
        //It is only a line shown. This has to be the length of a
        //Stem from a MelodyNote with Length Eighth
        flagRect = getRectForFlag(DOWN1);
        adjustFlagRectForNote(flagRect, note);
        break;
    case NoteLength::Eighth:
        flagRect = getRectForFlag(DOWN1);
        adjustFlagRectForNote(flagRect, note);
        m_svgRenderer->render(painter,
                              m_flagIds->value(DOWN1) + addition,
                              flagRect);
        break;
    case NoteLength::Sixteenth:
        flagRect = getRectForFlag(DOWN2);
        adjustFlagRectForNote(flagRect, note);
        m_svgRenderer->render(painter,
                              m_flagIds->value(DOWN2) + addition,
                              flagRect);
        break;
    case NoteLength::Thirtysecond:
        flagRect = getRectForFlag(DOWN3);
        adjustFlagRectForNote(flagRect, note);
        m_svgRenderer->render(painter,
                              m_flagIds->value(DOWN3) + addition,
                              flagRect);
        break;
    default:
        qDebug() << "StandardStemDrawer::drawStem - Length not covered";
    }
    t_pointBottom.setY(flagRect.bottom());
    if(length->length() != NoteLength::Whole){
        painter->drawLine(t_pointTop, t_pointBottom );
    }
}

void StandardStemDrawer::updateStems()
{
    update(m_rect); //Updates completet bounding rect
}

void StandardStemDrawer::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    event->ignore();
}

void StandardStemDrawer::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    event->ignore();
}

void StandardStemDrawer::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    event->ignore();
}

void StandardStemDrawer::keyPressEvent(QKeyEvent *event)
{
    qDebug() << "Keypress stemdraw";
    event->ignore();
}

void StandardStemDrawer::adjustFlagRectForNote(QRectF &flagBound, const MelodyNote *note)
{
    flagBound.moveTopLeft(mapFromParent( note->leftConnection().x()-0.5 ,note->pitch()->y()+0.9*m_lineHeight));
}
