/**
 * @file
 * @author  Thomas Baumann <teebaum@ymail.com>
 *
 * @section LICENSE
 *
 * <h3>GNU General Public License version 3</h3>
 * This program is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, see <http://www.gnu.org/licenses/>.
 *
 */
#ifndef MUSICBAR_H
#define MUSICBAR_H

#include <QGraphicsWidget>
#include <QList>

#include "itemtypes.h"
#include "clef.h"
#include "timesignature.h"

class PitchList;
class StemDrawer;
class Symbol;
class QGraphicsLinearLayout;

/*!
  * @class MusicBar
  * @brief This class is a standard music-bar.
  * @image html musicbar_coordinates.png "The coordinate-system of a MusicBar"
  *
  * It has a list with symbols. The symbols are arranged by a QGraphicsLinearLayout.
  * Its @ref StemDrawer draws the stems of the melody-notes in the list.
  *
  * @todo Do we need the symbols in the layout and in the list? Reconsider design.<br />
  * Issue: http://dev.limepipes.org/issues/13
  * ... addition: No we don't we need a custom layout for the symbols, which positions symbols by
  * their pitch, handles animation of symbols, and the signal-slot-connection between symbols when
  * they are inserted/removed. It should use the QProperty-system to check, if a symbol has a pitch,
  * a length. Also if it uses a pointer to follow/preceedPitch, every new symbol, that implements these
  * propertys can rely on the layout to set them right.
  */
class MusicBar : public QGraphicsWidget
{
    Q_OBJECT
public:
    /*!
      * @brief Constructor
      * @param scene The GraphicsScene
      * @param pitchList The @ref PitchList for the specific instrument (Great Higland Bagpipes for example)
      * @param pen The pen with wich the music-bar is drawn
      */
    MusicBar( QGraphicsScene *scene, const PitchList *pitchList, const Symbol *cursor, const QPen *pen = 0);
    //! Custom Item Typ itemtypes.h
    enum { Type = MusicBarType };
    //! Reimplemented from QGraphicsItem
    int type() const { return Type; }
    //! Reimplemented from QGraphicsItem
    QRectF boundingRect() const;
    //! Reimplemented from QGraphicsItem
    void paint( QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget );
    //! Reimplemented from QGraphicsLayoutItem
    void setGeometry(const QRectF &rect);

    //! Set pen for drawing the music-bar
    void setPen( const QPen *pen );

    /*! Inserts a symbol into the bar.
      * @param idx The index for the symbol to insert. Is the index 0, the symbol
      * is prepended. Is the index higher than the number of symbols, it is appended.
      * @param symbol The symbol to insert
      * @return true, if Symbol was inserted, false, if Symbol can't be inserted
      */
    void insert( int idx, Symbol *symbol);

    /*! Removes a Symbol from the bar. Disconnects its slots from signal of surrounding Symbols. */
    void removeSymbol( int idx );

    /*! Append a symbol to the bar
      * @note Only this function for appending Symbols is fully implemented yet.
      */
    void append( Symbol *symbol );

    /*! Append a list of symbols to the bar.
      * @note This function isn't fully implemented yet.
      */
    void append( QList<Symbol *>symbols);

    /*! Prepend a symbol to the bar.
      * @note This function isn't fully implemented yet.
      */
    void prepend( Symbol *symbol );

    /*! Prepend a list of symbols to the bar.
      * @note This function isn't fully implemented yet.
      */
    void prepend( QList<Symbol *>symbols );

    /*! @return baseline depending on margins */
    qreal baseLine() const {
        return m_margins;
    }
    /*! Sets the music cursor. */
    void setMusicCursor(const Symbol *sym){
        m_cursor = sym;
    }
    /*! Sets the Signal-Slots of two symbols among each other. */
    void connectSymbols( Symbol *left, Symbol *right);
    /*! Updates the layout according to the music symbols in m_symbols */
    void updateLayout();

    /*! Set the Clef for the MusicBar */
    void setClef(int type);

    /*! Sets the visibility of the clef */
    void setClefVisible(bool visible);

    /*! Set upbeat */
    void setUpbeat(bool upbeat);

public slots:
    void updateCursor(const Symbol *);
    /*! Set the TimeSignature for the MusicBar */
    void setTimeSig(int type);
    void setTuneTimeSig(const TimeSignature*timeSig);
    void setFirstSymOfNext(Symbol *symbol);
    void setLastSymOfPrev(Symbol *symbol);
    /*! Sets the timeSignature for the MusicBar */
    void updateTimeSig(QAction *action);
    /*! Updates the visibility of the timesignature depending on the Tune-TimeSig */
    void updateTimeSignature();
    /*! Sets the visibility of the clef */
    void setTimeSigVisible(bool visible);

signals:
    void lastSymChanged(Symbol *);
    void firstSymChanged(Symbol *);

private:
    //! The graphics scene
    QGraphicsScene *m_scene;
    //! @brief The list with the symbols in the bar
    QList<Symbol *>m_symbols;
    //! @brief The @ref PitchList for the specific instrument
    const PitchList *m_pitchList;
    //! @brief The @ref StemDrawer for the bar.
    StemDrawer *m_stemDrawer;
    //! @brief The bounding rectangle
    QRectF m_rect;
    //! @brief The pen for drawing the bar
    const QPen *m_pen;
    //! @brief The layout for the symbols
    QGraphicsLinearLayout *m_symbolLayout;

    /*! margin top and bottom */
    qreal m_margins;

    /*! The last Symbol of previous Bar */
    Symbol *m_lastSymPrev;
    /*! The first Symbol of next Bar */
    Symbol *m_firstSymNext;

    void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
    void hoverMoveEvent(QGraphicsSceneHoverEvent *event);
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void keyPressEvent(QKeyEvent *event);
    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event);

    /*! The actual index in symbols list, where the new symbol would be placed. */
    int m_cursorIdx;
    /*! The music cursor symbol */
    const Symbol *m_cursor;
    /*! The modifiable Symbol, which is drawn at m_cursorIdx. It is a copy of m_cursor,
      * because m_cursor is a const pointer. If the actual insert symbol is fixed in
      * the music bar by mouse click, a new symbol is allocated.
      */
    Symbol *m_insertSymbol;
    /*! Gets a copy of the cursor symbol and sets it to m_insertSymbol */
    void setInsertSymbolFromCursor();

    /*! The clef for the MusicBar */
    Clef *m_clef;

    /*! The TimeSignature for the MusicBar */
    TimeSignature *m_timeSig;

    /*! True, if the TimeSignature of the MusicBar differs from the Tune */
    bool m_timeSigDiffers;

    /*! The TimeSignature of the Tune */
    const TimeSignature *m_tuneTimeSig;

    /*! Is the clef visible or not */
    bool m_clefVisible;

    /*! Is the timesignature visible or not */
    bool m_timeSigVisible;

    /*! Space for non-layout symbols like TimeSignature or Clef */
    qreal m_spaceForNonLayoutSym;

    /*! Is Bar upbeat or not. Upbeats only take as much space as they need. */
    bool m_isUpbeat;

    QAction *timeSig22Action;
    QAction *timeSig24Action;
    QAction *timeSig34Action;
    QAction *timeSig44Action;
    QAction *timeSig38Action;
    QAction *timeSig68Action;
    QAction *timeSig98Action;
    QAction *timeSig128Action;
    QAction *timeSigVisibleAction;

    void createActions();
    void createConnections();

    /*! Sets the Position of the non-layout Symbols depending on which are visible */
    void setNonLayoutSymbolPositions();

    /*! Updates the width of the layout for clef and/or time-signature */
    void updateLayoutMargins();

    /*! If there is enough space for the symbol */
    bool spaceForSymbol(const Symbol *sym);

    /*! Only for debugging */
    void symbolsDebug();
};

#endif // MUSICBAR_H
