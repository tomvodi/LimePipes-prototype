/**
 * @file
 * @author  Thomas Baumann <teebaum@ymail.com>
 *
 * @section LICENSE
 *
 * <h3>GNU General Public License version 3</h3>
 * This program is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software Foundation;
 * either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, see <http://www.gnu.org/licenses/>.
 *
 */
#include "clef.h"
#include "pitchlist.h"

#include <QtGui>
#include <QSvgRenderer>
#include <QDebug>

Clef::Clef(const PitchList *pitchList, const QPen *pen)
    :Symbol(pitchList, pen)
{
    m_clefs = new QHash<int, QString>;
    m_clefs->insert(Clef::Treble, QString(":/mainwindow/images/clef_treble.svg"));
    m_clefs->insert(Clef::Bass, QString(":/mainwindow/images/clef_bass.svg"));
    m_clefs->insert(Clef::Alto, QString(":/mainwindow/images/clef_alto_tenor.svg"));
    m_clefs->insert(Clef::Tenor, QString(":/mainwindow/images/clef_alto_tenor.svg"));

    m_svgRenderer = new QSvgRenderer(m_clefs->value(m_clef), this);
    setClef(Treble);
}

Clef::~Clef(){
    delete m_svgRenderer;
}

void Clef::setClef(int clef)
{
    if(m_clef != clef){
        m_clef = clef;
        m_svgRenderer->load(m_clefs->value(clef));
    }
    setPreferredSize(getRectForClef().size());
}

void Clef::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    setVisible(true);
    m_svgRenderer->render(painter, getRectForClef());
}

QRectF Clef::boundingRect()
{
    qreal hpw = m_pen->width() / 2;
    QRectF rect = getRectForClef();
    return rect.adjusted( -hpw, -hpw, hpw, hpw );
}

QRectF Clef::getRectForClef() const
{
    QRectF t_rect;
    QSize t_size = m_svgRenderer->defaultSize(); //Defaultsize of clef-svg
    qreal heightToWidth = (qreal)t_size.height() / t_size.width(); //proportion of clef-svg
    switch (m_clef){
        case Clef::Treble: {
            qreal targetHeight = (qreal)m_pitchList->lineHeight() * 4 * 1.8;
            qreal targetWidth = targetHeight / heightToWidth;
            t_rect = QRectF(0, 27, targetWidth, targetHeight);
            break;
        }
        case Clef::Bass: {
            qreal targetHeight = (qreal)m_pitchList->lineHeight() * 3.3;
            qreal targetWidth = targetHeight / heightToWidth;
            t_rect = QRectF(0, m_pitchList->baseLine(), targetWidth, targetHeight);
            break;
        }
        case Clef::Alto: {
            qreal targetHeight = (qreal)m_pitchList->lineHeight() * 4;
            qreal targetWidth = targetHeight / heightToWidth;
            t_rect = QRectF(0, m_pitchList->baseLine(), targetWidth, targetHeight);
            break;
        }
        case Clef::Tenor: {
            qreal targetHeight = (qreal)m_pitchList->lineHeight() * 4;
            qreal targetWidth = targetHeight / heightToWidth;
            t_rect = QRectF(0, m_pitchList->baseLine()-m_pitchList->lineHeight(), targetWidth, targetHeight);
            break;
        }
        default: {
            qDebug() << "Clef not supported in Clef::getRectForClef()";
            break;
        }
    }
    return t_rect;
}
